$(document).ready(function() {
    $("select").multiselect({
        buttonWidth: '100%',
        includeSelectAllOption: true,
        selectAllText: 'wszystko',
        enableClickableOptGroups: true,
        buttonText: function(options, select) {
            if (options.length === 0) {
                return 'Wszystko';
            }
            else if(options.length === select.children().length) {
                return 'Wszystko';
            }
            else if (options.length >=  3) {
                return 'Wybrano '+options.length;
            }
            else {
                var labels = [];
                options.each(function() {
                    if ($(this).attr('label') !== undefined) {
                        labels.push($(this).attr('label'));
                    }
                    else {
                        labels.push($(this).html());
                    }
                });
                return labels.join(', ') + '';
            }
        }
    });
    $('.filters').css('visibility', 'visible');
    $('[data-toggle="popover"]').each(function(){
        popoverInitialize($(this));
    })
});


function popoverInitialize(obj)
{
    obj
    .popover({'content': absenceAction, 'html' : true})
    .on('inserted.bs.popover', function() {
        var href      = $(this).data('href');
        var absenceId = $(this).data('absence_id');
        var container = $('.popover-body');
        if($('#absence_reject_reason_submit').length !== 0) {
            rejectSubmit(container, href, absenceId);
        }
    }).on('hidden.bs.popover', function(){
        var absenceId = $(this).data('absence_id');
        var container = $('#absence_'+absenceId);
        $.ajax({
            type: "POST",
            url: 'absence_list/absence_card/'+absenceId,
            async: true,
            success: function(response){
                container.html(response).promise().done(function(){
                    container.find('[data-toggle="popover"]').each(function(){
                        popoverInitialize($(this));
                    });
                });
            }
        });
    });
}

function absenceAction()
{
    var href      = $(this).data('href');
    return $.ajax({
        type: "POST",
        url: href,
        async: false
    }).responseText;
}

function rejectSubmit(container, href, absenceId)
{
    var submit_btn = $('#absence_reject_reason_submit')
    submit_btn.click(function (e) {
        var form = $('form[name="absence_reject_reason"]');
        var formData = form.serialize();
        $('#wait').fadeIn('fast', function(){
            $.ajax({
                type: "POST",
                url: href,
                data: formData,
                success: function(rejectContent) {
                    container.html(rejectContent);
                    if($('.invalid-feedback').length > 0) {
                        $('.invalid-feedback').show();
                    }
                    if(submit_btn.length === 0) {

                    } else {
                        rejectSubmit(container, href, absenceId);
                    }
                }
            });
        });
        e.preventDefault()
    });
}


