$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
    $('.modal_action').click(function() {
        var btn = $(this);
        var url = btn.data('href');
        $.ajax({
            type: "POST",
            async: false,
            url: url,
            success: function(response) {
                $('#dictModal').modal({'show': true}).on('hidden.bs.modal', function(){
                    location.reload();
                });
                $('#dictModal').find('.modal-body').html(response).promise().done( function() {
                    modalForm(url);
                });
            }
        })
    });
});

function modalForm(url)
{
    $('#dictModal form').on('submit', function(e) {
        var data = $(this).serialize();
        $.ajax({
            type: "POST",
            async: false,
            url: url,
            data: data,
            success: function (response) {
                $('#dictModal').find('.modal-body').html(response).promise().done(function () {
                    modalForm(url);
                })
            }
        });
        e.preventDefault();
    });
}