$(document).ready(function () {
    $('button.delete').each(function(){
        popoverDelete($(this));
    });
    $('.modal_action').click(function() {
        $(this).tooltip('hide');
        var btn = $(this);
        var url = btn.data('href');
        $.ajax({
            type: "POST",
            async: false,
            url: url,
            data: {
                year: btn.attr('data-year')
            },
            success: function(response) {
                $('#holidayDictModal').modal({'show': true}).on('hidden.bs.modal', function(){
                    location.reload();
                });
                $('#holidayDictModal').find('.modal-body').html(response).promise().done( function() {
                    modalForm(url);
                });
            }
        })
    });
    $('.tooltip-on').tooltip();
});

function popoverDelete(obj) {
    obj.popover({'content': getDeleteContent, 'html' : true}).on('hidden.bs.popover', function() {
        obj.tooltip('hide').tooltip('dispose');
        $('.row[data-id="'+$(this).attr('data-id')+'"]').remove();
    });
}

function getDeleteContent() {
    return $.ajax({
        type: "POST",
        async: false,
        url: $(this).data('href')
    }).responseText;
}

function modalForm(url)
{
    $('#holidayDictModal form').on('submit', function(e) {
        var data = $(this).serialize();
        $.ajax({
            type: "POST",
            async: false,
            url: url,
            data: data,
            success: function (response) {
                $('#holidayDictModal').find('.modal-body').html(response).promise().done(function () {
                    modalForm(url);
                })
            }
        });
        e.preventDefault();
    })
}