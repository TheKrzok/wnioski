$(document).ready(function(){
    $('.user_delete').each(function(){
        popoverDelete($(this));
    });
    $('.modal_action').click(function() {
        var btn = $(this);
        var url = btn.data('href');
        $.ajax({
            type: "POST",
            async: false,
            url: url,
            success: function(response) {
                $('#listModal').modal({'show': true}).on('hidden.bs.modal', function(){
                    location.reload();
                });
                $('#listModal').find('.modal-body').html(response).promise().done( function() {
                    modalForm(url);
                });
            }
        })
    });
    $('.tooltip-on').tooltip();
});


function modalForm(url)
{
    $('#listModal form').on('submit', function(e) {
        var data = $(this).serialize();
        $.ajax({
            type: "POST",
            async: false,
            url: url,
            data: data,
            success: function (response) {
                $('#listModal').find('.modal-body').html(response).promise().done(function () {
                    modalForm(url);
                })
            }
        });
        e.preventDefault();
    })
}

function popoverDelete(obj) {
    obj.popover({'content': getDeleteContent, 'html' : true}).on('hidden.bs.popover', function() {
        location.reload();
    });

}

function getDeleteContent()
{
    return $.ajax({
        type: "POST",
        dataType: "json",
        async: false,
        url: $(this).data('href')
    }).responseText;
}