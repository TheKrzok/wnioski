$(document).ready(function() {
    $("select").multiselect({
        buttonWidth: '100%',
        includeSelectAllOption: true,
        selectAllText: 'wszystko',
        enableClickableOptGroups: true,
        buttonText: function(options, select) {
            if (options.length === 0) {
                return 'Wszystko';
            }
            else if(options.length === select.children().length) {
                return 'Wszystko';
            }
            else if (options.length >=  3) {
                return 'Wybrano '+options.length;
            }
            else {
                var labels = [];
                options.each(function() {
                    if ($(this).attr('label') !== undefined) {
                        labels.push($(this).attr('label'));
                    }
                    else {
                        labels.push($(this).html());
                    }
                });
                return labels.join(', ') + '';
            }
        }
    });
    $('.filters').css('visibility', 'visible');

    $('form[name="calendar_filter"]').on('submit', function(e) {
        form_submit($(this));
        e.preventDefault();
    }).submit();
});

function form_submit(form)
{
    $('#wait').fadeIn('normal', function(){
        var data = form.serialize();
        $.ajax({
            type: "POST",
            url: 'view',
            async: false,
            data: data,
            success: function(response) {
                $('#wait').fadeOut('normal', function(){
                    $('#calendar_data').html(response);
                    $('[data-toggle="tooltip"]').tooltip({'html':true});
                    $('[data-toggle="popover"]').each(function(){
                        popoverInitialize($(this));
                    });
                });
            }
        });
    });

}

function popoverInitialize(object) {
    object.popover({'content': get_popover_content, 'html' : true}).on('inserted.bs.popover', function() {
        var cell = $(this);
        $('#absence_accept, #absence_initial, #absence_reject, #absence_re-accept').on('click', function (){
            absenceAction($(this), cell);
        });
    }).on('click', function(){
        $('[data-toggle="popover"]').not(this).popover('hide');
    });
}


function absenceAction(button, cell) {
    button.siblings().addBack().fadeOut('fast');
        var href      = button.data('href');
        var container = button.parent();
        var status    = button.data('status');
        var absenceId = cell.data('absence_id');


        $('#wait').fadeIn('fast', function () {
            var content = $.ajax({
                type: "POST",
                url: href,
                async: false
            }).responseText;
            container.html(content);

            var cells = $('td[data-absence_id="'+absenceId+'"]');
            $('[data-toggle="popover"]').popover('update');

            if(status === 'accepted') {
                cells.removeClass('a-pending a-initial a-cancelled').attr('data-status', 'accepted').attr('data-toggle', 'tooltip').not(cell).tooltip().popover('dispose');
                cell.on('hidden.bs.popover', function(){
                    $(this).tooltip().popover('dispose');
                });
            } else if (status === 'initial') {
                cells.removeClass('a-pending').addClass('a-initial').attr('data-status', 'initial');
                popoverInitialize($('td[data-absence_id="'+absenceId+'"]').not(cell));
                cell.on('hidden.bs.popover', function(){
                    $(this).popover('dispose').attr('data-status', 'initial').off();
                    popoverInitialize($(this));
                });
            } else {
                reject_submit(container, href, absenceId, cell);
            }
        });
}

function reject_submit(container, href, absenceId, cell)
{
        $('#absence_reject_reason_submit').click(function (e) {
        var form = $('form[name="absence_reject_reason"]');
        var formData = form.serialize();
        $('#wait').fadeIn('fast', function(){
            $.ajax({
                type: "POST",
                url: href,
                data: formData,
                success: function(rejectContent) {
                    container.html(rejectContent).promise().done(function(){
                        if($('.invalid-feedback').length > 0) {
                            $('.invalid-feedback').show();
                        }
                        if(container.find('#absence_reject_reason_submit').length === 0) {
                            $('td[data-absence_id="'+absenceId+'"]')
                                .css('background', 'white')
                                .empty()
                                .removeClass('a-cell a-pending a-initial')
                                .not(cell).popover('dispose');
                            cell.on('hidden.bs.popover', function(){
                                $(this).popover('dispose');
                            });
                        } else {
                            reject_submit(container, href, absenceId);
                        }
                    });
                }
            });
        });

        e.preventDefault()
    });

}

function get_popover_content() {
    return $.ajax({
        type: "POST",
        dataType: "json",
        async: false,
        data: {
            'absenceId' : $(this).attr('data-absence_id'),
            'status' : $(this).attr('data-status'),
        },
        url: 'popover'
    }).responseText;
}


