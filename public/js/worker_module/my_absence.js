$(document).ready(function () {
    $('[data-toggle="popover"]').each(function(){
        popoverInitialize($(this));
    });
    $("select").multiselect({
        buttonWidth: '100%',
        includeSelectAllOption: true,
        selectAllText: 'wszystko',
        enableClickableOptGroups: true,
        buttonText: function(options, select) {
            if (options.length === 0) {
                return 'Wszystko';
            }
            else if(options.length === select.children().length) {
                return 'Wszystko';
            }
            else if (options.length >=  3) {
                return 'Wybrano '+options.length;
            }
            else {
                var labels = [];
                options.each(function() {
                    if ($(this).attr('label') !== undefined) {
                        labels.push($(this).attr('label'));
                    }
                    else {
                        labels.push($(this).html());
                    }
                });
                return labels.join(', ') + '';
            }
        }
    });
    $('.filters').css('visibility', 'visible');
});

function popoverInitialize(obj)
{
    obj
        .popover({'content': absenceAction, 'html' : true})
        .on('hidden.bs.popover', function(){
            if($(this).hasClass('absence-row')) {
                var absenceId = $(this).data('absence_id');
                var container = $('#absence_row_'+absenceId);
                $.ajax({
                    type: "POST",
                    url: 'absence_row/'+absenceId,
                    async: true,
                    success: function(response){
                        container.html(response);
                    }
                });
            } else if($(this).hasClass('absence-card')) {
                var absenceId = $(this).data('absence_id');
                var container = $('#absence_card_'+absenceId);
                $.ajax({
                    type: "POST",
                    url: 'absence_card/'+absenceId,
                    async: true,
                    success: function(response){
                        container.html(response);
                    }
                });
            }
        });
}

function absenceAction()
{
    var href      = $(this).data('href');
    return $.ajax({
        type: "POST",
        url: href,
        async: false
    }).responseText;
}