$(document).ready(function(){
    $('#absence_fromDate').on('change', function(){
        var val = $(this).val();
        if(val !== 'undefined') {
            $('#absence_toDate').val(val);
        }
        getGroupAbsences();
    });

    $('#absence_toDate').on('change', function(){
        getGroupAbsences();
    });
});

function getGroupAbsences()
{
    $('#group_absences').empty();
    $.ajax({
        type: "POST",
        dataType: "json",
        data: {
            'fromDate' : $('#absence_fromDate').val(),
            'toDate'   : $('#absence_toDate').val()
        },
        url: 'group_absences',
        success: function(response){
            console.log();
            if(true === response.dataFound) {
                $('#group_absences').html(response.htmlData);
            }
        }
    });
}
