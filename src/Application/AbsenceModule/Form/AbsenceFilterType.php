<?php
/**
 * Created by PhpStorm.
 * User: TheKrzok
 * Date: 02.02.2018
 * Time: 10:53
 */

namespace App\Application\AbsenceModule\Form;

use App\Entity\AbsenceDict;
use App\Entity\AbsenceStatusDict;
use App\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class AbsenceFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('year', DateType::class, array(
                'label' => 'Rok',
                'label_attr' => array('class' => 'p-0'),
                'widget' => 'single_text',
                'format' => 'yyyy',
                'required' => true,
                'constraints' => array(
                    new NotBlank(array('message' => 'Pole rok nie może być puste'))
                ),
            ))->add('user', EntityType::class, array(
                'class' => User::class,
                'placeholder' => 'wszyscy',
                'label' => 'Pracownik',
                'choice_label' => function($user) {
                    return $user->getFullName();
                },
                'required' => false,
                'query_builder' => function (EntityRepository $er) use ($options) {
                    $qb =  $er->createQueryBuilder('u');
                    if(empty($options['groups'])) {
                        $qb->setParameter('user', $options['user']);
                        $qb->where('u.superior = :user');
                    } else {
                        $qb->setParameter('groups', $options['groups']);
                        $qb->where($qb->expr()->in('u.group', ':groups'));
                    }
                    $qb->orderBy('u.surname', 'ASC');
                    return $qb;
                },
                'multiple' => true,
                'label_attr' => array('class' => 'p-0'),
            ))
            ->add('type', EntityType::class, array(
                'class' => AbsenceDict::class,
                'placeholder' => 'wszystkie',
                'label' => 'Typ',
                'choice_label' => 'description',
                'required' => false,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('t')
                        ->setParameter('active', 1)
                        ->where('t.active = :active')
                        ->orderBy('t.id', 'ASC');
                },
                'multiple' => true,
                'label_attr' => array('class' => 'p-0'),
            ))
            ->add('status', EntityType::class, array(
                'class' => AbsenceStatusDict::class,
                'placeholder' => 'wszystkie',
                'label' => 'Status',
                'choice_label' => 'description',
                'required' => false,
                'query_builder' => function (EntityRepository $er) {
                    $qb = $er->createQueryBuilder('s');
                    $qb->orderBy('s.id', 'ASC');
                    return $qb;
                },
                'multiple' => true,
                'label_attr' => array('class' => 'p-0'),
            ))
            ->add('submit', SubmitType::class, array(
                'label' => 'Filtruj',
                'attr' => array(
                    'class' => 'btn btn-success justify-content-center align-self-center'
                )
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array('user' => null, 'groups' => array()));
    }
}