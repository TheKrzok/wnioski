<?php
/**
 * Created by PhpStorm.
 * User: TheKrzok
 * Date: 11.06.2018
 * Time: 13:56
 */

namespace App\Application\AbsenceModule\Controller;

use App\Application\AbsenceModule\Form\AbsenceRejectReasonType;
use App\Application\AbsenceModule\Utils\AbsenceActionMail;
use App\Entity\Absence;
use App\Entity\AbsenceStatusDict;
use App\Service\MailerService;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;


class AbsenceActionController extends Controller
{
    /**
     * @Route("superior/absence_action/{id}/{status}", name="absence_action")
     * @Route("worker/absence_action/{id}/{status}", name="worker_absence_action")
     * @Route("absence_action_remote/{status}/securityCode={securityCode}", name="absence_action_remote")
     * @param Request $request
     * @param $id
     * @param null $status
     * @param $securityCode
     * @param AbsenceActionMail $absenceActionMail
     * @return mixed
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     * @throws \Exception
     */
    public function AbsenceAction(Request $request, $id = NULL, $status = NULL, $securityCode = NULL, AbsenceActionMail $absenceActionMail)
    {
        $message    = NULL;
        $alertClass = NULL;
        $formView   = NULL;
        $em         = $this->getDoctrine()->getManager();
        $back       = true;
        $remote     = false;

        if($securityCode !== NULL) {
            $absence = $em->getRepository(Absence::class)->findOneBy(array('securityCode' => $securityCode));
            $back   = false;
            $remote = true;
        } else {
            $absence = $em->getRepository(Absence::class)->find($id);
        }
        $newAbsence = $em->getRepository(AbsenceStatusDict::class)->findOneBy(array('name' => $status));

        if($absence) {
            $oldStatus      = $absence->getStatus();
            $oldName        = $oldStatus->getName();
            $oldDescription = $oldStatus->getDescription();
            $newName        = $status;
            $newDescription = $newAbsence->getDescription();
            $em             = $this->getDoctrine()->getManager();
            $user           = $absence->getUser();
            $superior       = $user->getSuperior();

            $absence->setStatus($em->getRepository(AbsenceStatusDict::class)->findOneBy(array('name' => $newName)));
            $absence->setStatusChangeDate(new DateTime());
            $em->merge($absence);

            $message    = "Wniosek zmienił status na ".$newDescription.". Na adres użytkownika " . $user->getSurname() . " " . $user->getName() . " została wysłana informacja o zmianie statusu wniosku";
            $alertClass = 'alert-info';

            if(in_array($oldName, array('pending', 'initial'))) {
                if($newName == 'rejected') {
                    $form = $this->createForm(AbsenceRejectReasonType::class, $absence, array('attr' => array('novalidate' => 'novalidate')));
                    $form->handleRequest($request);
                    $formView = $form->createView();

                    if($form->isSubmitted() && $form->isValid()) {

                        $sent = $absenceActionMail->sendNotifications($user, $superior, $absence);
                        if(false === $sent) {
                            $message = "Nie udało się wysłać powiadomień e-mail. Skontaktuj się z administratoerm.";
                            $alertClass = 'alert-danger';
                        } else {
                            $em->flush();
                        }
                        $formView = NULL;
                    }
                } else if($newName == 'withdrawn') {
                    if($absence->getFromDate() > new DateTime()) {
                        $message    = "Wniosek zmienił status na ".$newDescription.". Na adres użytkownika " . $superior->getSurname() . " " . $superior->getName() . " została wysłana informacja o zmianie statusu wniosku";
                        $sent = $absenceActionMail->sendNotifications($superior, $user, $absence, true);
                        if(false === $sent) {
                            $message = "Nie udało się wysłać powiadomień e-mail. Skontaktuj się z administratoerm.";
                            $alertClass = 'alert-danger';
                        } else {
                            $em->flush();
                        }
                    } else {
                        $message = 'Podana absencja już się rozpoczęła. Nie można jej wycofać';
                        $alertClass = 'alert-danger';
                    }

                } else {
                    $sent = $absenceActionMail->sendNotifications($user, $superior, $absence);
                    if(false === $sent) {
                        $message = "Nie udało się wysłać powiadomień e-mail. Skontaktuj się z administratoerm.";
                        $alertClass = 'alert-danger';
                    } else {
                        $em->flush();
                    }
                }
            }
            else if($oldName == 'accepted' && $newName == 'cancelled') {
                $message        = 'Została wyslana prośba o zatwierdzenie anulowania wniosku do przełożonego';
                $alertClass     = 'alert-info';
                $securityCode   = md5(random_bytes(10));
                $reAcceptLink   = $this->generateUrl("absence_action_remote", array('securityCode' => $securityCode, 'status' => 'accepted'), UrlGeneratorInterface::ABSOLUTE_URL);
                $absence->setSecurityCode($securityCode);
                $sent = $absenceActionMail->sendNotifications($superior, $user, $absence, true, $reAcceptLink);
                if(false === $sent) {
                    $message = "Nie udało się wysłać powiadomień e-mail. Skontaktuj się z administratoerm.";
                    $alertClass = 'alert-danger';
                } else {
                    $em->flush();
                }
            } else if($oldName == 'cancelled' && $newName == 'accepted') {
                $sent = $absenceActionMail->sendNotifications($user, $superior, $absence);
                if(false === $sent) {
                    $message = "Nie udało się wysłać powiadomień e-mail. Skontaktuj się z administratoerm.";
                    $alertClass = 'alert-danger';
                } else {
                    $em->flush();
                }
            }
            else {
                $message = "Podany wniosek jest w chwili obecnej na statusie ".$oldDescription.".
                Status pozostaje niezmieniony.";
                $alertClass = 'alert-info';
            }
        } else {
            $message = "Nie znaleziono wniosku. Skontaktuj się z administratoerm.";
            $alertClass = 'alert-danger';
        }

        return $this->render('@AbsenceModule/absence_action.html.twig', array('message' => $message, 'alertClass' => $alertClass, 'form' => $formView, 'back' => $back, 'remote' => $remote));

    }

}