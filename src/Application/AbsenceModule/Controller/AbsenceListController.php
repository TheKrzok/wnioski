<?php
/**
 * Created by PhpStorm.
 * User: TheKrzok
 * Date: 01.02.2018
 * Time: 22:00
 */

namespace App\Application\AbsenceModule\Controller;


use App\Application\AbsenceModule\Form\AbsenceFilterType;
use App\Entity\Absence;
use App\Entity\AbsenceDict;
use App\Entity\AbsenceStatusDict;
use App\Entity\GroupSupervisor;
use App\Entity\User;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AbsenceEvidenceController
 * @package App\Application\AbsenceModule\Controller
 */
class AbsenceListController extends AbstractController
{
    /**
     * @Route("absence_list", name="absence_list")
     * @param Request $request
     * @return Response
     */
    public function AbsenceListAction(Request $request)
    {

        $em                 = $this->getDoctrine()->getManager();
        $curDate            = new DateTime();
        $absenceConstraints = array();
        $groups             = array();
        $groupsArray        = $em->getRepository(GroupSupervisor::class)->findBy(array('supervisor' => $this->getUser()));
        $statusArray        = $em->getRepository(AbsenceStatusDict::class)->findBy(array('name' => array('initial', 'accepted', 'pending')));

        if(!empty($groupsArray)) {
            foreach($groupsArray AS $g) {
                $groups[] = $g->getGroup()->getId();
            }
        }

        $form = $this->createForm(
            AbsenceFilterType::class,
            array('year' => $curDate),
            array(
                'user'   => $this->getUser(),
                'groups' => $groups,
                'attr'   => array(
                    'novalidate' => 'novalidate',
                )
            )
        );

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $status = $form->get('status')->getData()->toArray();
            $user   = $form->get('user')->getData()->toArray();
            $type   = $form->get('type')->getData()->toArray();
            $year   = $form->get('year')->getData();


            if(false === empty($status)) {
                $absenceConstraints['status'] = $status;
            } else {
                $absenceConstraints['status'] = $statusArray;
            }

            if(false === empty($type)) {
                $absenceConstraints['type'] = $type;
            }

            if(null !== $year) {
                $absenceConstraints['year'] = (int)$year->format('Y');
            }

            if(false === empty($user)) {
                $absenceConstraints['user'] = $user;
            } else {
                if($this->isGranted('ROLE_SUPERIOR')) {
                    $absenceConstraints['user'] = $em->getRepository(User::class)->findBy(array('superior' => $this->getUser()->getId()));
                } elseif ($this->isGranted('ROLE_GROUP_SUPERVISOR')) {
                    $absenceConstraints['user'] = $em->getRepository(User::class)->findBy(array('group' => $groups));;
                }

            }

        }

        $absence   = $em->getRepository(Absence::class)->findBy($absenceConstraints, array('fromDate' => 'DESC'));

        return $this->render("@AbsenceModule/absence_list.html.twig", array('a' => $absence, 'form' => $form->createView()));
    }


    /**
     * @Route("absence_list/absence_card/{id}", name="absence_card")
     * @param Request $request
     * @param $id
     * @return Response|AccessDeniedException
     */
    public function AbsenceCardAction(Request $request, $id)
    {
        if($request->isXmlHttpRequest()) {
            $em      = $this->getDoctrine()->getManager();
            $absence = $em->getRepository(Absence::class)->find($id);

            return $this->render("@AbsenceModule/absence_card.html.twig", array('ab' => $absence));
        }

        throw $this->createAccessDeniedException();
    }

    /**
     * @Route("calendar/popover", name="calendar_popover")
     * @param Request $request
     * @return Response|AccessDeniedException
     */
    public function PopoverAction(Request $request)
    {
        if($request->isXmlHttpRequest()) {
            $absenceId = $request->get('absenceId');
            $status    = $request->get('status');
            return $this->render("@CalendarModule/popover.html.twig", array('absenceId' => $absenceId, 'status' => $status));
        }

        throw $this->createAccessDeniedException();
    }
}