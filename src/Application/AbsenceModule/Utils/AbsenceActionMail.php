<?php
/**
 * Created by PhpStorm.
 * User: TheKrzok
 * Date: 11.06.2018
 * Time: 14:29
 */

namespace App\Application\AbsenceModule\Utils;


use App\Entity\Absence;
use App\Entity\GroupSupervisor;
use App\Entity\User;
use App\Service\MailerService;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\ControllerTrait;
use Twig_Environment;

/**
 * Class AbsenceActionMail
 * @package App\Application\AbsenceModule\Utils
 */
class AbsenceActionMail
{

    protected $em;
    protected $ms;
    protected $te;

    public function __construct(EntityManager $em, Twig_Environment $te, MailerService $ms)
    {
        $this->em = $em;
        $this->ms = $ms;
        $this->te = $te;
    }

    /**
     * @param User $receiver
     * @param User $sender
     * @param Absence $absence
     * @param bool $inverse
     * @param null $link
     * @return bool
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function sendNotifications(User $receiver, User $sender, Absence $absence, $inverse = false, $link = null)
    {
        $subject  = 'Zmiana statusu wniosku';
        if(true === $inverse) {
            $group = $sender->getGroup();
        } else {
            $group = $receiver->getGroup();
        }
        $sentAll  = true;
        $template = $this->te->render('@AbsenceModule/mail_absence_status_change.html.twig', array('absence' => $absence, 'sender' => $sender, 'link' => $link));
        $supervisorArray = $this->em->getRepository(GroupSupervisor::class)->findBy(array('group' => $group));

        $sent = $this->ms->sendMail($subject, $receiver->getEmail(), $template);
        if(false === $sent) {
            $sentAll = false;
        }

        foreach($supervisorArray AS $s) {
            if($s->getSupervisor()->getId() !== $sender->getId()) {
                $template = $this->te->render('@AbsenceModule/mail_absence_status_change.html.twig', array('absence' => $absence, 'sender' => $sender, 'link' => null));
                $sent = $this->ms->sendMail($subject, $s->getSupervisor()->getEmail(), $template);
                if(false === $sent) {
                    $sentAll = false;
                }
            }
        }

        return $sentAll;

    }
}