<?php

namespace App\Application\AdminModule\Controller;

use App\Application\AdminModule\Form\UserType;
use App\Entity\User;
use App\Service\DateService;
use App\Service\MailerService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;


/**
 * Kontroler akcji rejestracji w serwisie
 *
 * Class UserController
 * @package App\Controller\SecurityModule
 */
class UserController extends AbstractController
{
    /**
     * Akcja rejestracji w serwisie
     *
     * @Route("admin/user/add/{type}", name="superior_add")
     * @Route("admin/user/add/{type}/{superiorId}", name="user_add")
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param MailerService $mailer
     * @param DateService $dateService
     * @param $type
     * @param int $superiorId
     * @return Response|AccessDeniedException
     * @throws \Exception
     */
    public function addAction(Request $request, UserPasswordEncoderInterface $passwordEncoder, MailerService $mailer, DateService $dateService, $type, $superiorId = 0)
    {
        if($request->isXmlHttpRequest()) {
            $user = new User();
            $message = '';
            $formDetails = array('headerName' => 'przełożonego', 'headerAdditional' => null);
            $superior = $this->getDoctrine()->getManager()->getRepository(User::class)->findOneBy(array('id' => $superiorId));
            if ($superior) {
                $formDetails['headerName'] = 'użytkownika';
                $formDetails['headerAdditional'] = " jako podwładnego użytkownika " . $superior->getName() . " " . $superior->getSurname();
            }

            $form = $this->createForm(UserType ::class, $user,
                array('attr' => array('novalidate' => 'novalidate'), 'type' => $type));
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $temporaryPassword = 'Arpus_' . random_int(1000, 9999);
                $password = $passwordEncoder->encodePassword($user, $temporaryPassword);
                $birthdateDate = $user->getBirthdate();
                $passwordExpiryDate = $dateService->getNowPlusMonth();


                $user->setPassword($password);
                $user->setBirthdate($birthdateDate);
                $user->setPasswordExpiryDate($passwordExpiryDate);

                if ('superior' === $type) {
                    $user->addRole('ROLE_SUPERIOR');
                } else {
                    $user->addRole('ROLE_WORKER');
                }

                if ($superior) {
                    $user->setSuperior($superior);
                }

                $user->setIsActive(true);

                $em = $this->getDoctrine()->getManager();
                $em->persist($user);
                $em->flush();

                $message = 'Użytkownik ' . $user->getUsername() . ' został dodany. Na adres e-mail ' . $user->getEmail() . ' została wysłana wiadomość z linkiem do strony logowania.';

                $mailer->sendMail('Witaj w systemie!', $user->getEmail(),
                    $this->renderView('@AdminModule/hello_mail.html.twig',
                        array('userName' => $user->getUsername(), 'temporaryPassword' => $temporaryPassword)));

                unset($form);
                unset($user);
                $user = new User();
                $form = $this->createForm(UserType ::class, $user,
                    array('attr' => array('novalidate' => 'novalidate'), 'type' => $type));
            }

            return $this->render('@AdminModule/user_add.html.twig',
                array('form' => $form->createView(), 'formDetails' => $formDetails, 'message' => $message));
        }

        throw $this->createAccessDeniedException();
    }

    /**
     * @Route("admin/user/edit/{id}/{type}", name="user_edit")
     *
     * @param Request $request
     * @param $id
     * @param $type
     * @return Response|AccessDeniedException
     */
    public function editAction(Request $request, $id, $type)
    {
        if($request->isXmlHttpRequest()) {
            $message = '';
            $em = $this->getDoctrine()->getManager();
            $user = $em->getRepository(User::class)->findOneBy(array('id' => $id));
            $form = $this->createForm(UserType ::class, $user, array('attr' => array('novalidate' => 'novalidate'), 'type' => $type));
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->merge($user);
                $em->flush();
                $message = 'Poprawnie zaktualizowano dane';
            }

            return $this->render("@AdminModule/user_edit.html.twig",
                array('form' => $form->createView(), 'userName' => $user->getUsername(), 'message' => $message));
        }

        throw $this->createAccessDeniedException();
    }

    /**
     * @Route("admin/user/delete/{id}", name="user_delete")
     * @param Request $request
     * @param $id
     * @return Response|AccessDeniedException
     */
    public function deleteAction(Request $request, $id)
    {
        if($request->isXmlHttpRequest()) {
            $em = $this->getDoctrine()->getManager();
            $user = $em->getRepository(User::class)->findOneBy(array('id' => $id));
            $em->remove($user);
            $em->flush();
            return $this->render("@AdminModule/user_list_delete.html.twig", array('user' => $user));
        }

        throw $this->createAccessDeniedException();
    }
}