<?php
/**
 * Created by PhpStorm.
 * User: TheKrzok
 * Date: 04.07.2018
 * Time: 14:37
 */

namespace App\Application\AdminModule\Controller;

use App\Application\AdminModule\Form\HolidayMatrixType;
use App\Entity\HolidayMatrix;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HolidayMatrixController extends AbstractController
{
    /**
     * @Route("admin/holiday/matrix", name="holiday_matrix")
     * @return Response
     */
    public function HolidayMatrixAction()
    {
        $em = $this->getDoctrine()->getManager();
        $holidayMatrix = $em->getRepository(HolidayMatrix::class)->findBy(array(), array('month' => 'ASC', 'day' => 'ASC'));

        return $this->render("@AdminModule/holiday_matrix.html.twig", array('holidays' => $holidayMatrix));
    }


    /**
     * @Route("admin/holiday/matrix/row")
     * @param Request $request
     * @return Response
     */
    public function HolidayMatrixRowAction(Request $request)
    {
        if($request->isXmlHttpRequest()) {
            $em = $this->getDoctrine()->getManager();
            $id = $request->get('id');
            $holidayRow = $em->getRepository(HolidayMatrix::class)->find($id);

            return $this->render("@AdminModule/holiday_matrix_row.html.twig", array('h' => $holidayRow));
        }

        throw $this->createAccessDeniedException();
    }

    /**
     * @Route("admin/holiday/matrix/delete/{id}", name="holiday_matrix_delete")
     * @param Request $request
     * @param $id
     * @return Response
     */
    public function DeleteAction(Request $request, $id)
    {
        if($request->isXmlHttpRequest()) {
            $em      = $this->getDoctrine()->getManager();
            $holiday = $em->getRepository(HolidayMatrix::class)->find($id);
            $em->remove($holiday);
            $em->flush();
            return $this->render("@AdminModule/row_delete.html.twig");
        }

        throw $this->createAccessDeniedException();
    }

    /**
     * @Route("admin/holiday/matrix/add", name="holiday_matrix_add")
     * @param Request $request
     * @return Response
     */
    public function AddAction(Request $request)
    {
        $holidayMatrix = new HolidayMatrix();
        $message = '';
        $class   = '';
        if($request->isXmlHttpRequest()) {
            $form = $this->createForm(HolidayMatrixType ::class, $holidayMatrix, array('attr' => array('novalidate' => 'novalidate')));
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $em    = $this->getDoctrine()->getManager();
                $day   = $holidayMatrix->getDay();
                $month = $holidayMatrix->getMonth();
                $year  = (int)date('Y');

                $holidayTest = $em->getRepository(HolidayMatrix::class)->findOneBy(array('day' => $day, 'month' => $month));

                if(!empty($holidayTest)) {
                    $message = 'W wybranym dniu istnieje już święto: '.$holidayTest->getDescription();
                    $class = 'danger';
                } else {
                    $dateCheck = checkdate($month, $day, $year);
                    if($dateCheck) {
                        $em->persist($holidayMatrix);
                        $em->flush();
                        $message = 'Poprawnie dodano święto';
                        $class = 'success';

                        unset($holidayMatrix);
                        unset($form);
                        $holidayMatrix = new HolidayMatrix();
                        $form = $this->createForm(HolidayMatrixType ::class, $holidayMatrix, array('attr' => array('novalidate' => 'novalidate')));
                    } else {
                        $message = 'Wybrana data jest niepoprawna';
                        $class = 'danger';
                    }
                }
            }

            return $this->render('@AdminModule/holiday_matrix_add.html.twig', array('form' => $form->createView(), 'message' => $message, 'class' => $class));

        }

        throw $this->createAccessDeniedException();
    }
}