<?php
/**
 * Created by PhpStorm.
 * User: TheKrzok
 * Date: 24.01.2018
 * Time: 13:58
 */

namespace App\Application\AdminModule\Controller;

use App\Entity\User;
use App\Entity\UserRole;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class WorkerListController
 * @package App\Application\AdminModule\Controller
 */
class UserListController extends AbstractController
{
    /**
     * @Route("admin/user_list", name="user_list")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function UserListAction()
    {
        $em = $this->getDoctrine()->getManager();
        $superiorCollection = $em->getRepository(UserRole::class)->findBy(array('role' => 'ROLE_SUPERIOR'));

        foreach($superiorCollection AS $s) {
            $sup = $s->getUser();
            foreach($sup AS $ss) {
                $superiorArray[$ss->getId()]['superior'] = $ss;
                $superiorArray[$ss->getId()]['inferior'] = $ss->getInferior();
            }
        }

        return $this->render("@AdminModule/user_list.html.twig", array('superiorArray' => $superiorArray));
    }
}