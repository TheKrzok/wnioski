<?php
/**
 * Created by PhpStorm.
 * User: TheKrzok
 * Date: 24.01.2018
 * Time: 13:44
 */

namespace App\Application\AdminModule\Controller;

use App\Application\AdminModule\Form\AbsenceDictType;
use App\Entity\AbsenceDict;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AbsenceDictController
 * @package App\Application\AdminModule\Controller
 */
class AbsenceDictController extends AbstractController
{
    /**
     * @Route("admin/absence_dict", name="absence_dict")
     * @return Response
     */
    public function ViewAction()
    {
        $em = $this->getDoctrine()->getManager();
        $absenceDict = $em->getRepository(AbsenceDict::class)->findBy(array('active' => 1));

        return $this->render("@AdminModule/absence_dict.html.twig", array('absenceDict' => $absenceDict));
    }


    /**
     * @Route("admin/absence_dict/new", name="absence_dict_new")
     * @param Request $request
     * @return Response|AccessDeniedException
     */
    public function NewAction(Request $request)
    {
        if($request->isXmlHttpRequest()) {
            $message = '';
            $absence = new AbsenceDict();
            $form = $this->createForm(AbsenceDictType::class, $absence,
                array('attr' => array('novalidate' => 'novalidate')));
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $absence->setActive(1);
                $em->persist($absence);
                $em->flush();

                $absence = new AbsenceDict();
                $form = $this->createForm(AbsenceDictType::class, $absence, array('attr' => array('novalidate' => 'novalidate')));
                $message = 'Poprawnie zaktualizowano słownik';
            }

            return $this->render("@AdminModule/absence_dict_new.html.twig", array('form' => $form->createView(), 'message' => $message));
        }

        throw $this->createAccessDeniedException();

    }

    /**
     * @Route("admin/absence_dict/delete/{id}", name="absence_dict_delete")
     * @param $id
     * @return RedirectResponse
     */
    public  function DeleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $absence = $em->getRepository(AbsenceDict::class)->findOneBy(array('id' => $id));
        $absence->setActive(0);
        $em->merge($absence);
        $em->flush();

        return $this->redirectToRoute("absence_dict");
    }

    /**
     * @Route("admin/absence_dict/edit/{id}", name="absence_dict_edit")
     * @param Request $request
     * @param $id
     * @return Response|AccessDeniedException
     */
    public function EditAction(Request $request, $id)
    {
        if($request->isXmlHttpRequest()) {

            $message = '';
            $em      = $this->getDoctrine()->getManager();
            $absence = $em->getRepository(AbsenceDict::class)->findOneBy(array('id' => $id));
            $form    = $this->createForm(AbsenceDictType::class, $absence, array('attr' => array('novalidate' => 'novalidate')));
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $em->merge($absence);
                $em->flush();
                $message = 'Poprawnie zaktualizowano słownik';
            }

            return $this->render("@AdminModule/absence_dict_edit.html.twig",
                array('absence' => $absence, 'form' => $form->createView(), 'message' => $message));
        }

        throw $this->createAccessDeniedException();
    }
}