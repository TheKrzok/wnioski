<?php
/**
 * Created by PhpStorm.
 * User: TheKrzok
 * Date: 05.07.2018
 * Time: 13:17
 */

namespace App\Application\AdminModule\Controller;


use App\Application\AdminModule\Form\HolidayDictType;
use App\Application\AdminModule\Form\YearType;
use App\Entity\Calendar;
use App\Entity\CalendarDay;
use App\Entity\CalendarDayDict;
use App\Entity\HolidayDict;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HolidayDictController extends AbstractController
{
    /**
     * @Route("admin/holiday/dict", name="holiday_dict")
     * @param Request $request
     * @return Response
     */
    public function HolidayDictAction(Request $request)
    {
        $em       = $this->getDoctrine()->getManager();
        $year     = date('Y');
        $dt       = new DateTime();
        $formYear = $dt::createFromFormat('Y', $year);
        $form     = $this->createForm(YearType::class, array('year' => $formYear), array('attr' => array('novalidate' => 'novalidate', 'class' => 'form-inline')));
        $calendar = $em->getRepository(Calendar::class)->findBy(array('year' => $year));
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $year = (int)$data['year']->format('Y');
            $calendar = $em->getRepository(Calendar::class)->findBy(array('year' => $year));
        }

        $holidayDict = $em->getRepository(HolidayDict::class)->findBy(array('calendar' => $calendar), array('day' => 'ASC'));

        return $this->render("@AdminModule/holiday_dict.html.twig", array('holidays' => $holidayDict, 'year' => $year, 'form' => $form->createView()));
    }

    /**
     * @Route("admin/holiday/dict/delete/{id}", name="holiday_dict_delete")
     * @param Request $request
     * @param $id
     * @return Response
     */
    public function DeleteAction(Request $request, $id)
    {
        if($request->isXmlHttpRequest()) {
            $em          = $this->getDoctrine()->getManager();
            $holiday     = $em->getRepository(HolidayDict::class)->find($id);
            $calendarDay = $em->getRepository(CalendarDay::class)->findOneBy(array('holiday' => $holiday));

            $cDay    = $calendarDay->getDay();
            $weekDay = $cDay->format('N');

            if(in_array($weekDay,array(6,7))) {
                $dayTypeLetter = 'W';
            } else {
                $dayTypeLetter = 'N';
            }

            $dayType = $em->getRepository(CalendarDayDict::class)->findOneBy(array('dayType' => $dayTypeLetter));
            $calendarDay->setHoliday(null);
            $calendarDay->setDayType($dayType);
            $em->merge($calendarDay);
            $em->remove($holiday);
            $em->flush();

            return $this->render("@AdminModule/row_delete.html.twig");
        }

        throw $this->createAccessDeniedException();
    }

    /**
     * @Route("admin/holiday/dict/add", name="holiday_dict_add")
     * @param Request $request
     * @return Response
     */
    public function AddAction(Request $request)
    {
        $message = '';
        $class   = '';
        $year    = $request->get('year');

        if($request->isXmlHttpRequest()) {
            $holidayDict = new HolidayDict();
            $form = $this->createForm(HolidayDictType ::class, $holidayDict, array('year' => $year, 'attr' => array('novalidate' => 'novalidate')));
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $em   = $this->getDoctrine()->getManager();
                $day  = $holidayDict->getDay();



                $calendarYear = $form['year']->getData();
                $holidayCheck = $em->getRepository(HolidayDict::class)->findOneBy(array('day' => $day));

                if(true === !empty($holidayCheck)) {
                    $message = 'W wybranym dniu istnieje już święto: '.$holidayCheck->getDescription();
                    $class   = 'danger';
                } else {
                    $calendar = $em->getRepository(Calendar::class)->findOneBy(array('year' => $calendarYear));
                    if(true === empty($calendar)) {
                        $message = 'Brak wygenerowanego kalendarza dla roku '.$calendarYear.'. Proszę najpierw wygenerować kalendarz';
                        $class   = 'danger';
                    } else {
                        $holidayDict->setCalendar($calendar);
                        $em->persist($holidayDict);
                        $em->flush();

                        $dayType     = $em->getRepository(CalendarDayDict::class)->findOneBy(array('dayType' => 'S'));
                        $calendarDay = $em->getRepository(CalendarDay::class)->findOneBy(array('day' => $day));
                        $calendarDay->setHoliday($holidayDict);
                        $calendarDay->setDayType($dayType);
                        $em->persist($calendarDay);
                        $em->flush();

                        $message = 'Poprawnie dodano święto';
                        $class = 'success';
                    }
                }
            }

            return $this->render('@AdminModule/holiday_dict_add.html.twig', array('form' => $form->createView(), 'message' => $message, 'class' => $class, 'year' => $year));
        }

        throw $this->createAccessDeniedException();
    }

}