<?php
/**
 * Created by PhpStorm.
 * User: TheKrzok
 * Date: 20.06.2018
 * Time: 13:42
 */

namespace App\Application\AdminModule\Controller;


use App\Application\AdminModule\Form\YearType;
use App\Entity\Calendar;
use App\Entity\CalendarDay;
use App\Entity\CalendarDayDict;
use App\Entity\HolidayDict;
use App\Entity\HolidayMatrix;
use DateInterval;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class CalendarController extends AbstractController
{
    /**
     * @Route("admin/calendar/generate", name="calendar_generate")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function GenerateAction(Request $request)
    {
        $em    = $this->getDoctrine()->getManager();
        $form  = $this->createForm(YearType::class, array('attr' => array('novalidate' => 'novalidate')));

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {

            $holidayArray = array();
            $dayType      = array();

            $data = $form->getData('year');
            $year = (int)$data['year']->format('Y');

            $calendarExists = $em->getRepository(Calendar::class)->findOneBy(array('year' => $year));

            if(!empty($calendarExists)) {
                $this->addFlash('error', 'Już istnieje kalendarz na rok '.$year);
            } else {
                $holidayMatrix = $em->getRepository(HolidayMatrix::class)->findAll();
                $calendar = new Calendar();
                $calendar->setYear($year);
                $em->persist($calendar);
                $em->flush();

                foreach($holidayMatrix AS $h) {
                    $holidayDict = new HolidayDict();
                    $month      = str_pad((string)$h->getMonth(),2, '0', STR_PAD_LEFT);
                    $day        = str_pad((string)$h->getDay(),2, '0', STR_PAD_LEFT);
                    $dateString = $year.'-'.$month.'-'.$day.' 00:00:00';

                    $dbDate = DateTime::createFromFormat('Y-m-d H:i:s', $dateString);

                    $holidayDict->setCalendar($calendar);
                    $holidayDict->setDay($dbDate);
                    $holidayDict->setDescription($h->getDescription());
                    $em->persist($holidayDict);
                    $em->flush();
                }

                $base   = new DateTime("$year-03-21");
                $days   = easter_days($year);
                $easter = $base->add(new DateInterval("P{$days}D"));

                $easterDb          = clone $easter;
                $easterMondayBase  = clone $easter;
                $easterMondayDb    = $easterMondayBase->add(new DateInterval("P1D"));
                $greenHolidayBase  = clone $easter;
                $greenHolidayDb    = $greenHolidayBase->add(new DateInterval("P49D"));
                $corpusChristiBase = clone $greenHolidayDb;
                $corpusChristiDb   = $corpusChristiBase->add(new DateInterval("P11D"));

                $corpusChristi = new HolidayDict();
                $corpusChristi->setCalendar($calendar);
                $corpusChristi->setDay($corpusChristiDb);
                $corpusChristi->setDescription('Boże ciało');
                $em->persist($corpusChristi);
                $em->flush();

                $easterDay = new HolidayDict();
                $easterDay->setCalendar($calendar);
                $easterDay->setDay($easterDb);
                $easterDay->setDescription('Wielkanoc');
                $em->persist($easterDay);
                $em->flush();

                $easterMonday = new HolidayDict();
                $easterMonday->setCalendar($calendar);
                $easterMonday->setDescription('Poniedziałek Wielkanocny');
                $easterMonday->setDay($easterMondayDb);
                $em->persist($easterMonday);
                $em->flush();

                $greenHoliday = new HolidayDict();
                $greenHoliday->setCalendar($calendar);
                $greenHoliday->setDay($greenHolidayDb);
                $greenHoliday->setDescription('Zesłanie Ducha Świętego (Zielone Świątki)');
                $em->persist($greenHoliday);
                $em->flush();

                $holidayDict  = $em->getRepository(HolidayDict::class)->findBy(array('calendar' => $calendar));

                $dayTypeDict  = $em->getRepository(CalendarDayDict::class)->findAll();
                $start        = new \DateTime($year.'-01-01 00:00:00');
                $end          = new \DateTime($year.'-12-31 00:00:00');

                foreach($dayTypeDict AS $d) {
                    $dayType[$d->getDayType()] = $d;
                }

                foreach($holidayDict AS $h) {
                    $holidayArray[$h->getDay()->getTimestamp()] = $h;
                }

                for($s=$start; $s<=$end; $s->add(new DateInterval('P1D'))) {
                    $month     = $s->format('n');
                    $weekDay   = $s->format('N');
                    $week      = $s->format('W');
                    $dayNumber = $s->format('j');
                    $quarter   = $quarter = ceil($month/3);
                    $tst       = $s->getTimestamp();

                    $calendarDay = new CalendarDay();
                    $calendarDay->setCalendar($calendar);

                    $calendarDay->setMonth($month);
                    $calendarDay->setWeek($week);
                    $calendarDay->setWeekDay($weekDay);
                    $calendarDay->setDay($s);
                    $calendarDay->setDayNumber($dayNumber);
                    $calendarDay->setQuarter($quarter);

                    if(isset($holidayArray[$tst])) {
                        $calendarDay->setHoliday($holidayArray[$tst]);
                        $calendarDay->setDayType($dayType['S']);
                    } else if(in_array($weekDay,array(6,7))) {
                        $calendarDay->setDayType($dayType['W']);
                    } else {
                        $calendarDay->setDayType($dayType['N']);
                    }

                    $em->persist($calendarDay);
                    $em->flush();
                }
                $this->addFlash('success', 'Poprawnie wygenerowano kalendarz na rok '.$year);
                $form  = $this->createForm(YearType::class, array('attr' => array('novalidate' => 'novalidate')));
            }
        }

        return $this->render("@AdminModule/calendar_generate.html.twig", array('form' => $form->createView()));
    }

}