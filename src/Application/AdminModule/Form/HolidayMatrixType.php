<?php
/**
 * Created by PhpStorm.
 * User: TheKrzok
 * Date: 05.07.2018
 * Time: 12:06
 */

namespace App\Application\AdminModule\Form;


use App\Entity\HolidayMatrix;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Range;

class HolidayMatrixType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('month', IntegerType::class, array(
                'label' => 'Miesiąc',
                'constraints' => array(
                    new NotBlank(array('message' => 'Miesiąc nie może być pusty')),
                    new Range(array(
                        'min' => 1,
                        'minMessage' => 'Miesiąc nie może być miejszy od 1',
                        'max' => 12,
                        'maxMessage' => 'Miesiąc nie może być większy od 12',
                    ))
                ),
                'attr' => array(
                    'min' => "1",
                    'max' => "12"
                )
            ))
            ->add('day', IntegerType::class, array(
                'label' => 'Dzień',
                'constraints' => array(
                    new NotBlank(array('message' => 'Dzień nie może być pusty')),
                    new Range(array(
                        'min' => 1,
                        'minMessage' => 'Dzień nie może być miejszy od 1',
                        'max' => 31,
                        'maxMessage' => 'Dzień nie może być większy od 31',
                    ))
                ),
                'attr' => array(
                    'min' => "1",
                    'max' => "31"
                )
            ))
            ->add('description', TextareaType::class, array(
                'label' => 'Opis',
                'constraints' => array(
                    new NotBlank(array('message' => 'Opuis nie może być pusty')),
                    new Length(array(
                        'min' => 5,
                        'minMessage' => 'Opis nie może być krótszy niż 5 znaków',
                        'max' => 150,
                        'maxMessage' => 'Opis nie może być dłuższy niż 150 znaków'
                    ))
                ),
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => HolidayMatrix::class,
        ));
    }
}