<?php
/**
 * Created by PhpStorm.
 * User: TheKrzok
 * Date: 25.01.2018
 * Time: 08:56
 */

namespace App\Application\AdminModule\Form;


use App\Entity\AbsenceDict;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ColorType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;

class AbsenceDictType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('description', TextType::class, array(
                'label' => 'Opis nieobecności',
                'constraints' => array(
                    new Length(array(
                        'min' => 4,
                        'minMessage' => 'Opis nie może być krótszy niż 4 znaki',
                        'max' => 30,
                        'maxMessage' => 'Opis nie może być dłuższy niż 30 znaków'
                    )),
                    new NotBlank(array(
                        'message' => 'Opis nie może być pusty'
                    ))
                )
            ))
            ->add('code',TextType::class, array(
                'label' => 'Kod',
                'attr' => array(
                    'style' => 'width: 5em;',
                    'maxlength' => '5',
                ),
                'constraints' => array(
                    new Length(array(
                        'min' => 2,
                        'minMessage' => 'Kod nie może być krótszy niż 2 znaki',
                        'max' => 5,
                        'maxMessage' => 'Kod nie może być dłuższy niż 5 znaków'
                    )),
                    new NotBlank(array(
                        'message' => 'To pole jest wymagane'
                    )),
                    new Regex(array(
                        'pattern' => '/^[A-Z]{2,5}$/',
                        'message' => 'Kod może się składać wytłącznie z dużych liter'
                    ))
                )
            ))
            ->add('color',ColorType::class, array(
                'label' => 'Kolor',
                'attr' => array(
                    'style' => 'width: 5em; height: 3em;',
                    'maxlength' => '10',
                    'type' => 'color',
                ),
                'constraints' => array(
                    new NotBlank(array(
                        'message' => 'To pole jest wymagane'
                    )),
                )
            ))
            ->add('limitable', CheckboxType::class, array(
                'label' => 'Limit',
                'attr' => array('checked' => 'true'),
            ))

            ->add('submit', SubmitType::class, array(
                'label' => 'Zapisz',
                'attr' => array('class' => 'btn btn-success'),
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => AbsenceDict::class,
        ));
    }
}