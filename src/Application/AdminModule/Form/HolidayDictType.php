<?php
/**
 * Created by PhpStorm.
 * User: TheKrzok
 * Date: 05.07.2018
 * Time: 14:51
 */

namespace App\Application\AdminModule\Form;


use App\Entity\HolidayDict;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\Range;

class HolidayDictType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('day', DateType::class, array(
                'label' => 'Dzień',
                'widget' => 'single_text',
                'input' => 'datetime',
                'constraints' => array(
                    new NotNull(array('message' => 'Data od nie może być pusta')),
                    new Date(array('message' => 'Podana data jest niepoprawna')),
                ),
                'attr' => array(
                    'min' => $options['year'].'-01-01',
                    'max' => $options['year'].'2019-12-31',
                )
            ))
            ->add('description', TextareaType::class, array(
                'label' => 'Opis',
                'constraints' => array(
                    new NotBlank(array('message' => 'Opuis nie może być pusty')),
                    new Length(array(
                        'min' => 5,
                        'minMessage' => 'Opis nie może być krótszy niż 5 znaków',
                        'max' => 150,
                        'maxMessage' => 'Opis nie może być dłuższy niż 150 znaków'
                    ))
                ),
            ))
            ->add('year', HiddenType::class, array(
                'data' => $options['year'],
                'mapped' => false,
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => HolidayDict::class,
            'year' => date('Y')
        ));
    }

}