<?php
/**
 * Created by PhpStorm.
 * User: TheKrzok
 * Date: 28.01.2018
 * Time: 13:18
 */

namespace App\Application\AdminModule\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Class YearType
 * @package App\Application\AdminModule\Form
 */
class YearType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('year', DateType::class, array(
                'label' => 'Rok',
                'widget' => 'single_text',
                'format' => 'yyyy',
                'constraints' => array(
                    new NotBlank(array('message' => 'Pole rok nie może być puste'))
                )
            ))
        ;
    }
}