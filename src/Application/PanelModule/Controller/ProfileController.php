<?php

namespace App\Application\PanelModule\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Kontroler akcji związanych z profilem użytkownika
 *
 * Class UserProfileController
 * @package App\Controller\SecurityModule
 */
class ProfileController extends AbstractController
{
    /**
     * Akcja wyświetle nia profilu użytkownika
     *
     * @Route("panel/profile", name="user_profile")
     * @return Response
     */
    public function ViewProfileAction()
    {
        $user = $this->getUser();

        return $this->render("@PanelModule/view_profile.html.twig", array('user' => $user));
    }
}