<?php
/**
 * Created by PhpStorm.
 * User: TheKrzok
 * Date: 05.06.2018
 * Time: 11:52
 */

namespace App\Application\PanelModule\Controller;


use App\Entity\Absence;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\DateService;

class PanelController extends AbstractController
{

    /**
     * @Route("panel", name="panel")
     * @return Response
     */
    public function PanelAction()
    {
        $variablesArray = array();
        $inferior       = array();
        if($this->isGranted('ROLE_WORKER')) {
            $pendingCount = 0;
            $pendingData = $em = $this->getDoctrine()->getManager()->getRepository(Absence::class)->getUserAbsenceCount($this->getUser()->getId(),0,0,'pending');
            if($pendingData) {
                $pendingCount = $pendingData[0]['count'];
            }
            $variablesArray['pendingCount'] = $pendingCount;
        }

        if($this->isGranted('ROLE_ABSENCE_LIST')) {
            if($this->isGranted('ROLE_SUPERIOR')) {
                $inferior = $this->getUser()->getInferior();
            } else if($this->isGranted('ROLE_GROUP_SUPERVISOR')) {
                $inferior = $this->getUser()->getSuperior()->getInferior();
            }

            $pendingCount = 0;
            if($inferior) {
                foreach($inferior as $i)
                {
                    $pendingData = $em = $this->getDoctrine()->getManager()->getRepository(Absence::class)->getUserAbsenceCount($i->getId(),0,0,'pending');
                    if($pendingData) {
                        $pendingCount += $pendingData[0]['count'];
                    }
                }
            }
            $variablesArray['superiorPendingCount'] = $pendingCount;
        }

        if($this->isGranted('ROLE_ADMIN')) {
            $dateService = new DateService();
            $absenceLimitsYear = $dateService->getCurrentYear();
            $variablesArray['absenceLimitsYear'] = $absenceLimitsYear;
        }

        $passwordExpiryDate = $this->getUser()->getPasswordExpiryDate();
        $variablesArray['passwordExpiryDate'] = $passwordExpiryDate;


        return $this->render('@PanelModule/panel.html.twig', array('variables' => $variablesArray));

    }
}