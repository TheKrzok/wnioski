<?php
/**
 * Created by PhpStorm.
 * User: TheKrzok
 * Date: 01.02.2018
 * Time: 22:02
 */

namespace App\Application\SuperiorModule\Controller;


use App\Entity\Absence;
use App\Entity\AbsenceDict;
use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class InferiorAbsenceLimitController
 * @package App\Application\SuperiorModule\Controller
 */
class InferiorAbsenceLimitController extends AbstractController
{
    /**
     * @Route("superior/inferior_absence_limit", name="inferior_absence_limit")
     * @return Response
     */
    public function inferiorAbsenceLimitAction()
    {

        $limitSum       = array();
        $absenceSum     = array();
        $em             = $this->getDoctrine()->getManager();
        $users          = $this->getUser()->getInferior();
        $aDict          = $em->getRepository(AbsenceDict::class)->findBy(array('limitable' => true, 'active' => true));
        $limitData      = $em->getRepository(User::class)->findAbsenceTypeLimitSum();

        if($limitData) {
            foreach($limitData AS $ld) {
                $limitSum[$ld['userId']][$ld['typeId']] = $ld['limitSum'];
                $absenceData = $em->getRepository(Absence::class)->findUserAbsenceSum($ld['userId'], $ld['typeId']);
                if($absenceData) {
                    foreach ($absenceData AS $ad) {
                        $absenceSum[$ad['userId']][$ad['typeId']] = $ad['sum'];
                    }
                } else {
                    $absenceSum[$ld['userId']][$ld['typeId']] = 0;
                }
            }
        }



        return $this->render("@SuperiorModule/inferior_absence_limit.html.twig", array('users' => $users, 'aDict' => $aDict, 'lSum' => $limitSum, 'aSum' => $absenceSum));
    }
}