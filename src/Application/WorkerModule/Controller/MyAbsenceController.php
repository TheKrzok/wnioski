<?php
/**
 * Created by PhpStorm.
 * User: TheKrzok
 * Date: 24.01.2018
 * Time: 10:24
 */

namespace App\Application\WorkerModule\Controller;

use App\Application\WorkerModule\Form\AbsenceFilterType;
use App\Entity\Absence;
use App\Entity\AbsenceDict;
use App\Entity\AbsenceStatusDict;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MyAbsenceController extends Controller
{
    /**
     * @Route("worker/my_absence", name="my_absence")
     * @param Request $request
     * @return Response
     */
    public function MyAbsenceAction(Request $request)
    {
        $em      = $this->getDoctrine()->getManager();
        $curDate = new DateTime();
        $curYear = (int)$curDate->format('Y');

        $form = $this->createForm(
            AbsenceFilterType::class,
            array('year' => $curDate),
            array(
                'attr' => array(
                    'novalidate' => 'novalidate',
                )
            ));

        $form->handleRequest($request);

        $user                   = $this->getUser();
        $constraints            = array();
        $constraints['user']    = $user;
        $constraints['year']    = $curYear;

        if($form->isSubmitted() && $form->isValid())
        {
            $status = $form->get('status')->getData()->toArray();
            $type   = $form->get('type')->getData()->toArray();
            $year   = $form->get('year')->getData();


            if(false === empty($status)) {
                $constraints['status'] = $status;
            }

            if(false === empty($type)) {
                $constraints['type'] = $type;
            }

            if(null !== $year) {
                $constraints['year'] = (int)$year->format('Y');
            }
        }

        $absence   = $em->getRepository(Absence::class)->findBy($constraints, array('fromDate' => 'DESC'));
        $paginator = $this->get('knp_paginator');

        $result = $paginator->paginate(
            $absence,
            $request->query->getInt('page', 1),
            $request->query->getInt('limit', 8)
        );


        return $this->render("@WorkerModule/my_absence.html.twig", array('a' => $result, 'form' => $form->createView()));
    }

    /**
     * @Route("worker/absence_row/{id}")
     * @param Request $request
     * @param $id
     * @return Response|AccessDeniedException
     */
    public function AbsenceRowAction(Request $request, $id)
    {
        if($request->isXmlHttpRequest()) {
            $em      = $this->getDoctrine()->getManager();
            $absence = $em->getRepository(Absence::class)->find($id);

            return $this->render("@WorkerModule/absence_row.html.twig", array('ab' => $absence));
        }

        throw $this->createAccessDeniedException();
    }

    /**
     * @Route("worker/absence_card/{id}")
     * @param Request $request
     * @param $id
     * @return Response|AccessDeniedException
     */
    public function AbsenceCardAction(Request $request, $id)
    {
        if($request->isXmlHttpRequest()) {
            $em      = $this->getDoctrine()->getManager();
            $absence = $em->getRepository(Absence::class)->find($id);

            return $this->render("@WorkerModule/absence_card.html.twig", array('ab' => $absence));
        }

        throw $this->createAccessDeniedException();
    }
}