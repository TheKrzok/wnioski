<?php
/**
 * Created by PhpStorm.
 * User: TheKrzok
 * Date: 30.01.2018
 * Time: 15:14
 */

namespace App\Application\WorkerModule\Controller;


use App\Entity\Absence;
use App\Entity\AbsenceDict;
use App\Entity\AbsenceStatusDict;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AbsenceLimitController extends AbstractController
{
    /**
     * @Route("worker/absence_limit", name="absence_limit")
     * @return Response
     */
    public function AbsenceLimitAction()
    {
        $userId = $this->getUser()->getId();

        $absenceLimit       = array();
        $absenceLimitSum    = array();
        $absenceDays        = array();

        $em                 = $this->getDoctrine()->getManager();
        $absenceDict        = $em->getRepository(AbsenceDict::class)->findBy(array('limitable' => true, 'active' => true));
        $absenceStatusDict  = $em->getRepository(AbsenceStatusDict::class)->findByNot('name', array('cancelled', 'rejected', 'withdrawn'));
        $absenceLimitData   = $this->getUser()->getAbsenceLimit();


        $icons = array(
            'pending'   => array(
                'icon'  => 'fas fa-hourglass-half',
                'color' => 'text-primary',
            ),
            'accepted'  => array(
                'icon'  => 'fas fa-thumbs-up',
                'color' => 'text-success',
            ),
            'initial'   => array(
                'icon'  => 'fas fa-handshake',
                'color' => 'text-warning',
            ),
        );

        if($absenceDict) {

            foreach($absenceDict AS $ad) {
                $absenceDays[$ad->getId()]['sum'] = 0;
                foreach ($absenceStatusDict AS $asd) {
                    $absenceDays[$ad->getId()][$asd->getId()]['sum'] = 0;
                }
            }

            foreach ($absenceLimitData AS $ald) {
                $typeId         = $ald->getType()->getId();
                $limitValue     = $ald->getLimitValue();
                $year           = $ald->getYear();

                $absenceLimit[$year][$typeId] = $limitValue;

                if(!isset($absenceLimitSum[$typeId])) {
                    $absenceLimitSum[$typeId] = $limitValue;
                } else {
                    $absenceLimitSum[$typeId] += $limitValue;
                }

                $absenceDays[$typeId][$year]['sum'] = 0;

                foreach ($absenceStatusDict AS $asd) {
                    $statusId   = $asd->getId();

                    $absenceDays[$typeId][$year][$statusId] = 0;

                    $absenceSum = $em->getRepository(Absence::class)->findUserAbsenceSum($userId, $typeId, $year, $statusId);

                    if($absenceSum) {
                        $absenceDays[$typeId][$year][$statusId] += $absenceSum[0]['sum'];
                        $absenceDays[$typeId][$year]['sum']     += $absenceSum[0]['sum'];
                        $absenceDays[$typeId]['sum']            += $absenceSum[0]['sum'];
                        $absenceDays[$typeId][$statusId]['sum'] += $absenceSum[0]['sum'];
                    }
                }
            }
        }

        return $this->render(
            "@WorkerModule/absence_limit.html.twig",
            array(
                'aDict'       => $absenceDict,
                'aLimit'      => $absenceLimit,
                'aLimitSum'   => $absenceLimitSum,
                'aDays'       => $absenceDays,
                'aStatusDict' => $absenceStatusDict,
                'i'           => $icons
            )
        );
    }
}