<?php
/**
 * Created by PhpStorm.
 * User: TheKrzok
 * Date: 24.01.2018
 * Time: 10:24
 */

namespace App\Application\WorkerModule\Controller;


use App\Application\WorkerModule\Form\AbsenceType;
use App\Entity\Absence;
use App\Entity\AbsenceDay;
use App\Entity\AbsenceStatusDict;
use App\Entity\Calendar;
use App\Entity\GroupSupervisor;
use App\Entity\HolidayDict;
use App\Entity\User;
use App\Service\MailerService;
use DateInterval;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Translation\Translator;

/**
 * Class NewAbsenceController
 * @package App\Application\WorkerModule\Controller
 */
class NewAbsenceController extends AbstractController
{
    /**
     * @Route("worker/new_absence", name="new_absence")
     * @param Request $request
     * @param MailerService $mailerService
     * @return Response
     * @throws \Exception
     */
    public function NewAbsenceAction(Request $request, MailerService $mailerService)
    {
        $absence = new Absence();
        $form    = $this->createForm(AbsenceType::class, $absence, array('attr' => array('novalidate' => 'novalidate')));
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {

            $from           = $absence->getFromDate();
            $to             = $absence->getToDate();
            $user           = $this->getUser();
            $userId         = $user->getId();
            $absenceType    = $absence->getType();
            $typeId         = $absenceType->getId();
            $em             = $this->getDoctrine()->getManager();
            $superiorMail   = $user->getSuperior()->getEmail();
            $group          = $user->getGroup();
            $yearInit       = (int)$from->format('Y');
            $errors         = array();
            $holidayArray   = array();
            $daysLeft       = 0;




            $groupSupervisors = $em->getRepository(GroupSupervisor::class)->findBy(array('group' => $group));
            $calendar         = $em->getRepository(Calendar::class)->findOneBy(array('year' => $yearInit));
            $holidays         = $calendar->getHolidayDict();

            foreach($holidays AS $h) {
                $holidayArray[] = $h->getDay();
            }

            $days = 0;

            $startDate = clone $from;
            $limitDate = clone $to;
            $daysArray = array();

            for ($startDate; $startDate <= $limitDate; $startDate->add(new DateInterval('P1D'))) {
                $weekday = (int)$startDate->format('N');
                if (!in_array($weekday, array(6, 7)) && (false === array_search($startDate, $holidayArray))) {
                    $daysArray[] = clone $startDate;
                    $days++;
                }
            }

            if ($to < $from) {
                $errors[] = 'Data do nie może być mniejsza od daty od';
            }
            else {
                $constraints = array();
                $constraints['userId']   = $userId;
                $constraints['fromDate'] = $from->format('Y-m-d');
                $constraints['toDate']   = $to->format('Y-m-d');

                $currentAbsences = $em->getRepository(Absence::class)->findUserAbsencesBetweenDates($constraints);
                if($currentAbsences) {
                    $errors[] = 'Na wybrany okres '.$from->format('d-m-Y').' - '.$to->format('d-m-Y')." istnieją już nieobnecności lub oczekujące wnioski:";
                    foreach ($currentAbsences AS $abs) {
                        $errors[] = '- '.$abs->getType()->getDescription().' od '.$abs->getFromDate()->format('d-m-Y').' do '.$abs->getToDate()->format('d-m-Y');
                    }
                }

                if ($absenceType->getLimitable()) {
                    $limit  = $em->getRepository(User::class)->getAbsenceLimit($userId, $typeId);
                    if (!$limit) {
                        $errors[] = 'Nie masz ustalonego limitu dla nieobecności "' . $absenceType->getDescription() . '" Skontaktuj się z administratorem';
                    }
                    else {
                        $usedDaysData = $em->getRepository(Absence::class)->findUserAbsenceSum($user->getId(), $absenceType->getId());
                        if($usedDaysData) {
                            $used = $usedDaysData[0]['sum'];
                        } else {
                            $used = 0;
                        }
                        $daysLeft   = $limit - $used;

                        if ($days > $daysLeft) {
                            $errors[] = 'Liczba dni na wniosku przekroczy dostępny limit dla nieobecności \''
                                .$absenceType->getDescription() . '\'.'
                                . ' Dni do wybrania: ' . $daysLeft
                                . ' Dni na wniosku: ' . $days;
                        }
                    }
                }

            }

            if(empty($errors)) {
                $status         = $em->getRepository(AbsenceStatusDict::class)->findOneBy(array('name' => 'pending'));
                $securityCode   = md5(random_bytes(10));

                $absence->setSecurityCode($securityCode);
                $absence->setYear($yearInit);
                $absence->setDays($days);
                $absence->setStatus($status);
                $absence->setUser($user);
                $absence->setCreateDate(new DateTime());

                foreach($daysArray AS $d) {
                    $absenceDay = new AbsenceDay();
                    $absenceDay->setAbsenceDay($d);
                    $absenceDay->setAbsence($absence);
                    $em->persist($absenceDay);
                }

                $em->persist($absence);
                $em->flush();

                $initialLink    = $this->generateUrl("absence_action_remote", array('securityCode' => $securityCode, 'status' => 'initial'), UrlGeneratorInterface::ABSOLUTE_URL);
                $acceptanceLink = $this->generateUrl("absence_action_remote", array('securityCode' => $securityCode, 'status' => 'accepted'), UrlGeneratorInterface::ABSOLUTE_URL);
                $rejectLink     = $this->generateUrl("absence_action_remote", array('securityCode' => $securityCode, 'status' => 'rejected'), UrlGeneratorInterface::ABSOLUTE_URL);

                $dataArray = array(
                    'iLink'     => $initialLink,
                    'aLink'     => $acceptanceLink,
                    'rLink'     => $rejectLink,
                    'u'         => $user,
                    'a'         => $absence,
                    'daysLeft'  => $daysLeft,
                    'functions' => true,
                );

                $subject = 'Nowy wniosek urlopowy od '.$user->getSurname().' '.$user->getName();

                $mailerService->sendMail(
                    $subject,
                    $superiorMail,
                    $this->render(
                        '@WorkerModule/mail_superior_absence_action.html.twig',
                        $dataArray
                    )
                );

                foreach($groupSupervisors AS $s) {
                    if($userId !== $s->getSupervisor()->getId()) {
                        $mail = $s->getSupervisor()->getEmail();
                        $dataArray['functions'] = false;
                        $mailerService->sendMail(
                            $subject,
                            $mail,
                            $this->render(
                                '@WorkerModule/mail_superior_absence_action.html.twig',
                                $dataArray
                            )
                        );
                    }
                }

                $this->addFlash('success', 'Wniosek został pomyślnie zapisany i oczekuje na zatwierdzenie przez przełożonego.');
            }
            else {
                $this->addFlash('error', implode("\n", $errors));
            }
        }

        return $this->render("@WorkerModule/new_absence.html.twig", array('form' => $form->createView()));
    }

    /**
     * @Route("worker/group_absences", name="group_absences")
     * @param Request $request
     * @return JsonResponse
     */
    public function GroupAbsencesAction(Request $request)
    {
        $responseArray = array();
        $constraints   = array();
        $constraints['fromDate'] = $request->get('fromDate');
        $constraints['toDate']   = $request->get('toDate');
        $constraints['groupId']  = $this->getUser()->getGroup()->getId();

        $absences = $this->getDoctrine()->getManager()->getRepository(Absence::class)->findUserAbsencesBetweenDates($constraints);

        if($absences) {
            $responseArray['dataFound'] = true;
            $responseArray['htmlData']  = $this->render("@WorkerModule/group_absences.html.twig", array('absences' => $absences))->getContent();
        } else {
            $responseArray['dataFound'] = false;
        }

        $response = new JsonResponse();
        $response->setData($responseArray);

        return $response;
    }
}