<?php
/**
 * Created by PhpStorm.
 * User: TheKrzok
 * Date: 30.01.2018
 * Time: 16:57
 */

namespace App\Application\WorkerModule\Form;

use App\Entity\AbsenceDict;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotNull;

/**
 * Class AbsenceType
 * @package App\Application\WorkerModule\Form
 */
class AbsenceType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('type', EntityType::class, array(
                'label' => 'Typ nieobecności',
                'class' => AbsenceDict::class,
                'query_builder' => function(EntityRepository $er) {
                    return
                        $er->createQueryBuilder('ad')
                        ->andWhere('ad.active = true')
                        ->orderBy('ad.id', 'ASC');
                },
                'choice_label' => 'description',
            ))
            ->add('fromDate', DateType::class, array(
                'label' => 'Od',
                'widget' => 'single_text',
                'input' => 'datetime',
                'constraints' => array(
                    new NotNull(array('message' => 'Data od nie może być pusta')),
                    new Date(array('message' => 'Podana data jest niepoprawna')),
                ),
                'attr' => array(
                    'style' => 'width: auto'
                ),
            ))
            ->add('toDate', DateType::class, array(
                'label' => 'Do',
                'widget' => 'single_text',
                'input' => 'datetime',
                'constraints' => array(
                    new NotNull(array('message' => 'Data do nie może być pusta')),
                    new Date(array('message' => 'Podana data jest niepoprawna')),
                ),
                'attr' => array(
                    'style' => 'width: auto'
                ),
            ))
            ->add('commentary', TextareaType::class, array(
                'label' => 'Komentarz',
                'constraints' => array(
                    new Length(array(
                        'max' => 255,
                        'maxMessage' => 'Komentarz nie może być dłuższy niż 255 znaków'
                    ))
                )
            ))
        ;
    }
}