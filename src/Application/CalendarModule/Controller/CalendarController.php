<?php
/**
 * Created by PhpStorm.
 * User: TheKrzok
 * Date: 12.06.2018
 * Time: 09:09
 */

namespace App\Application\CalendarModule\Controller;


use App\Application\CalendarModule\Form\CalendarFilterType;
use App\Entity\Absence;
use App\Entity\AbsenceDay;
use App\Entity\AbsenceStatusDict;
use App\Entity\Calendar;
use App\Entity\CalendarDay;
use App\Entity\GroupSupervisor;
use App\Entity\User;
use DateInterval;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class CalendarController extends AbstractController
{
    private $month = array(
        1 => 'Styczeń',
        2 => 'Luty',
        3 => 'Marzec',
        4 => 'Kwiecień',
        5 => 'Maj',
        6 => 'Czerwiec',
        7 => 'Lipiec',
        8 => 'Sierpień',
        9 => 'Wrzesień',
        10 => 'Październik',
        11 => 'Listopad',
        12 => 'Grudzień',
    );

    /**
     * @Route("calendar/view", name="calendar_view")
     * @param Request $request
     * @return Response
     */
    public function ViewAction(Request $request)
    {
        $em            = $this->getDoctrine()->getManager();
        $dataArray     = array();
        $groups        = array();
        $selectedUser  = array();
        $curDate       = new DateTime();

        if($this->isGranted('ROLE_GROUP_SUPERVISOR')) {
            $groupsArray = $em->getRepository(GroupSupervisor::class)->findBy(array('supervisor' => $this->getUser()));
            if(!empty($groupsArray)) {
                foreach($groupsArray AS $g) {
                    $groups[] = $g->getGroup()->getId();
                }
            }
        } else if ($this->isGranted('ROLE_WORKER')) {
            $groups[] = $this->getUser()->getGroup();
            $selectedUser = array($this->getUser());
        }

        $form = $this->createForm(
            CalendarFilterType::class,
            array(
                'year' => $curDate,
                'user'   => $this->getUser(),
                'groups' => $groups,
                'attr'   => array(
                    'novalidate' => 'novalidate',
                ),
                'selected_user' => $selectedUser,
            )
        );

        if($request->isXmlHttpRequest()) {
            $calendarArray   = array();
            $absenceArray    = array();
            $userConstraints = array();
            $statusArray     = $em->getRepository(AbsenceStatusDict::class)->findBy(array(
                'name' => array(
                    'initial',
                    'accepted',
                    'pending',
                    'cancelled'
                )
            ));

            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {

                $status = $form->get('status')->getData()->toArray();
                $user = $form->get('user')->getData();
                $type = $form->get('type')->getData()->toArray();
                $year = $form->get('year')->getData();

                if (false === empty($status)) {
                    $absenceConstraints['status'] = $status;
                } else {
                    $absenceConstraints['status'] = $statusArray;
                }

                if (false === empty($type)) {
                    $absenceConstraints['type'] = $type;
                }

                if (null !== $year) {
                    $absenceConstraints['year'] = (int)$year->format('Y');
                }

                if (false === empty($user)) {
                    $userConstraints['id'] = $user;
                } else {
                    if ($this->isGranted('ROLE_SUPERIOR')) {
                        $userConstraints['superior'] = $this->getUser()->getId();
                    } else {
                        $userConstraints['group'] = $groups;
                    }
                }

                $calendar     = $em->getRepository(Calendar::class)->findOneBy(array('year' => (int)$year->format('Y')));
                $calendarDays = $em->getRepository(CalendarDay::class)->findBy(array('calendar' => $calendar));//
                $absenceData  = $em->getRepository(Absence::class)->findBy($absenceConstraints);
                $usersData    = $em->getRepository(User::class)->findBy($userConstraints, array('surname' => 'ASC'));


                foreach ($absenceData AS $ad) {
                    $user        = $ad->getUser();
                    $absenceDays = $ad->getAbsenceDays();
                    foreach ($absenceDays AS $add) {
                        $day       = $add->getAbsenceDay();
                        $dayNumber = $day->format('j');
                        $month     = $day->format('n');
                        $absenceArray[$user->getId()][$month][$dayNumber] = $ad;
                    }
                }

                foreach ($calendarDays AS $cd) {

                    $month = $cd->getMonth();
                    $day   = $cd->getDay()->format('j');
                    $calendarArray[$month][$day] = $cd;
                }

                $dataArray['calendar'] = $calendarArray;
                $dataArray['months']   = $this->month;
                $dataArray['absence']  = $absenceArray;
                $dataArray['users']    = $usersData;
            }

            return $this->render("@CalendarModule/data.html.twig", $dataArray);
        }

        $dataArray['form'] = $form->createView();

        return $this->render("@CalendarModule/view.html.twig", $dataArray);
    }

    /**
     * @Route("calendar/popover", name="calendar_popover")
     * @param Request $request
     * @return Response|AccessDeniedException
     */
    public function PopoverAction(Request $request)
    {
        if($request->isXmlHttpRequest()) {
            $absenceId = $request->get('absenceId');
            $status    = $request->get('status');
            return $this->render("@CalendarModule/popover.html.twig", array('absenceId' => $absenceId, 'status' => $status));
        }

        throw $this->createAccessDeniedException();
    }



}