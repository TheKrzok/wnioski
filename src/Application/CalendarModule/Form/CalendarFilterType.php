<?php
/**
 * Created by PhpStorm.
 * User: TheKrzok
 * Date: 02.02.2018
 * Time: 10:53
 */

namespace App\Application\CalendarModule\Form;

use App\Entity\AbsenceDict;
use App\Entity\AbsenceStatusDict;
use App\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class CalendarFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('year', DateType::class, array(
                'label' => 'Rok',
                'label_attr' => array('class' => 'p-0'),
                'widget' => 'single_text',
                'format' => 'yyyy',
                'required' => true,
                'constraints' => array(
                    new NotBlank(array('message' => 'Pole rok nie może być puste'))
                ),
            ))->add('user', EntityType::class, array(
                'class' => User::class,
                'placeholder' => 'wszyscy',
                'label' => 'Pracownik',
                'choice_label' => function($user) {
                    return $user->getFullName();
                },
                'required' => false,
                'query_builder' => function (EntityRepository $er) use ($options) {
                    $qb =  $er->createQueryBuilder('u');
                        if(empty($options['data']['groups'])) {
                            $qb->setParameter('user', $options['data']['user']);
                            $qb->where('u.superior = :user');
                        } else {
                            $qb->setParameter('groups', $options['data']['groups']);
                            $qb->where($qb->expr()->in('u.group', ':groups'));
                        }
                        $qb->orderBy('u.surname', 'ASC');
                    return $qb;
                },
                'multiple' => true,
                'label_attr' => array('class' => 'p-0'),
                'data' => $options['data']['selected_user']
            ))
            ->add('type', EntityType::class, array(
                'class' => AbsenceDict::class,
                'placeholder' => 'wszystkie',
                'label' => 'Typ',
                'choice_label' => 'description',
                'required' => false,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('t')
                        ->setParameter('active', 1)
                        ->orderBy('t.description', 'ASC')
                        ->where('t.active = :active');
                },
                'multiple' => true,
                'label_attr' => array('class' => 'p-0'),
            ))
            ->add('status', EntityType::class, array(
                'class' => AbsenceStatusDict::class,
                'placeholder' => 'wszystkie',
                'label' => 'Status',
                'choice_label' => 'description',
                'required' => false,
                'query_builder' => function (EntityRepository $er) {
                    $qb = $er->createQueryBuilder('s');
                    $qb->setParameter('names', array('initial', 'accepted', 'pending', 'cancelled'));
                    $qb->add('where', $qb->expr()->in('s.name', ':names'));
                    $qb->orderBy('s.id', 'ASC');
                    return $qb;
                },
                'multiple' => true,
                'attr' => array(
                    'class' => 'text-left'
                ),
                'label_attr' => array('class' => 'p-0'),
            ))
            ->add('submit', SubmitType::class, array(
                'label' => 'Filtruj',
                'attr' => array(
                    'class' => 'btn btn-success justify-content-center align-self-center'
                )
            ))
            ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array('user' => null, 'groups' => array()));
    }
}