<?php
namespace App\Application\SecurityModule\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

/**
 * Kontorler akcji wylogowania użytkowanika
 *
 * Class LogoutController
 * @package App\Controller\SecurityModule
 */
class LogoutController extends AbstractController
{
    /**
     * Akcja wylogowania użytkownika
     *
     * @Route("security/logout", name="logout")
     * @return Response
     */
    public function logoutAction(){
        return $this->redirectToRoute('logout');
    }

}