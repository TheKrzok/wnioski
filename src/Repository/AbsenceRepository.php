<?php

namespace App\Repository;

use App\Entity\Absence;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class AbsenceRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Absence::class);
    }

    public function findUserAbsenceSum($userId=0, $typeId=0, $year=0, $statusId=0)
    {
        $qb = $this->createQueryBuilder('a');
        $qb->join('a.user', 'u');
        $qb->join('a.type', 'at');
        $qb->join('a.status', 'ast');
        $qb->addSelect('SUM(a.days) AS sum');

        if($userId != 0) {
            $qb->addSelect('u.id AS userId');
            $qb->andWhere('u.id = :userId')->setParameter('userId', $userId);
            $qb->addGroupBy('u.id');
        }

        if($typeId != 0) {
            $qb->addSelect('at.id AS typeId');
            $qb->andWhere('a.type = :typeId')->setParameter('typeId', $typeId);
            $qb->addGroupBy('at.id');
        }

        if($year != 0) {
            $qb->addSelect('a.year AS year');
            $qb->andWhere('a.year = :year')->setParameter('year', $year);
            $qb->addGroupBy('a.year');
        }

        if($statusId != 0) {
            $qb->addSelect('ast.name AS statusName');
            $qb->addSelect('ast.id AS statusId');
            $qb->andWhere('ast.id = :statusId')->setParameter('statusId', $statusId);
            $qb->addGroupBy('ast.id');
            $qb->addGroupBy('ast.name');
        }

        $qb->addOrderBy('a.year', 'ASC');
        $qb->andWhere($qb->expr()->notIn('ast.name', array('cancelled', 'rejected', 'withdrawn')));

        $result =  $qb->getQuery()->getResult();

        return $result;
    }

    public function getUserAbsenceCount($userId, $typeId=0, $year=0, $statusName='')
    {
        $qb = $this->createQueryBuilder('a');
        $qb->join('a.type', 'at');
        $qb->join('a.status', 'ast');
        $qb->addSelect('COUNT(a.days) AS count');
        $qb->andWhere('a.user = :userId')->setParameter('userId', $userId);

        if($typeId != 0)
        {
            $qb->addSelect('at.id AS typeId');
            $qb->andWhere('a.type = :typeId')->setParameter('typeId', $typeId);
            $qb->addGroupBy('at.id');
        }

        if($year != 0)
        {
            $qb->addSelect('a.year');
            $qb->andWhere('a.year = :year')->setParameter('year', $year);
            $qb->addGroupBy('a.year');
            $qb->addOrderBy('a.year', 'ASC');
        }

        if($statusName != '')
        {
            $qb->addSelect('ast.name AS statusName');
            $qb->andWhere('ast.name = :statusName')->setParameter('statusName', $statusName);
            $qb->addGroupBy('ast.name');
        }
        $result =  $qb->getQuery()->getResult();

        return $result;
    }

    public function findAbsences($constraints)
    {
        $qb = $this->createQueryBuilder('a');
        $qb->addSelect('a');
        $qb->join('a.type', 'at');
        $qb->join('a.status', 'ast');
        $qb->join('a.user', 'u');

        if(isset($constraints['superior'])) {
            $qb->andWhere('u.superior = :superior')->setParameter('superior', $constraints['superior']);
        }

        if(isset($constraints['year'])) {
            $qb->andWhere('a.year = :year')->setParameter('year', $constraints['year']);
        }

        if(isset($constraints['type'])) {
            $qb->andWhere('at.id = :type')->setParameter('type', $constraints['type']);
        }

        if(isset($constraints['status'])) {
            $qb->andWhere('ast.id = :status')->setParameter('status', $constraints['status']);
        }

        if(isset($constraints['user'])) {
            $qb->andWhere('a.user = :user')->setParameter('user', $constraints['user']);
        }

        $qb->addOrderBy('a.createDate', 'DESC');

        $result =  $qb->getQuery()->getResult();

        return $result;
    }

    public function findUserAbsencesBetweenDates($constraints)
    {

        $qb = $this->createQueryBuilder('a');

        $qb->addSelect('a');
        $qb->join('a.user', 'u');
        $qb->join('a.status', 'st');
        $qb->setParameter('fromDate', $constraints['fromDate']);
        $qb->setParameter('toDate', $constraints['toDate']);

        if(isset($constraints['userId'])){
            $qb->setParameter('userId', $constraints['userId']);
            $qb->andWhere('a.user = :userId');
        }

        if(isset($constraints['groupId'])) {
            $qb->setParameter('groupId', $constraints['groupId']);
            $qb->andWhere('u.group = :groupId');
        }

        $qb->setParameter('statusName', array('pending', 'initial', 'accepted'));
        $qb->andWhere('a.fromDate BETWEEN :fromDate AND :toDate OR a.toDate BETWEEN :fromDate AND :toDate OR :fromDate BETWEEN a.fromDate AND a.toDate OR :toDate BETWEEN a.fromDate AND a.toDate');
        $qb->andWhere($qb->expr()->in('st.name', ':statusName'));
        $qb->orderBy('a.fromDate', 'ASC');
        $qb->orderBy('u.surname', 'ASC');

        return $qb->getQuery()->getResult();
    }
}
