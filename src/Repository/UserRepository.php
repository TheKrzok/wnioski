<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class UserRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, User::class);
    }

    public function findAllSuperiors()
    {
        return $this
            ->createQueryBuilder('u')
            ->leftJoin('u.userRole', 'ur')
            ->where('ur.role = :role')
            ->setParameter('role', 'ROLE_SUPERIOR')
            ->orderBy('u.surname', 'ASC')
            ->getQuery()
            ->getResult()
            ;
    }

    public function findAbsenceLimitsYear($year)
    {
        return $this
            ->createQueryBuilder('u')
            ->leftJoin('u.absenceLimit', 'al')
            ->where('al.year = :year')
            ->setParameter('year', $year)
            ->orderBy('al.type', 'ASC')
            ->getQuery()
            ->getResult()
            ;
    }

    public function getAbsenceLimit($userId, $typeId)
    {
            return $this
                ->createQueryBuilder('u')
                ->join('u.absenceLimit', 'al')
                ->andWhere('u.id = :userId')
                ->setParameter('userId', $userId)
                ->andWhere('al.type = :typeId')
                ->setParameter('typeId', $typeId)
                ->select('SUM(al.limitValue) AS limitsSum')
                ->getQuery()
                ->getSingleScalarResult();
    }

    public function findAbsenceTypeLimitSum($userId=0, $typeId=0)
    {
        $qb = $this->createQueryBuilder('u');
        $qb->join('u.absenceLimit', 'al');
        $qb->join('al.type', 'at');

        $qb->addSelect('SUM(al.limitValue) AS limitSum');
        $qb->addSelect('u.id AS userId');
        $qb->addSelect('at.id AS typeId');

        if($userId != 0) {
            $qb->setParameter('userId', $userId);
            $qb->andWhere('u.id = :userId');
        }

        if($typeId != 0) {
            $qb->setParameter('typeId', $typeId);
            $qb->andWhere('at.id = :typeId');
        }

        $qb->addGroupBy('u.id');
        $qb->addGroupBy('al.type');

        return $qb->getQuery()->getArrayResult();

    }
}
