<?php

namespace App\DataFixtures;

use App\Entity\AbsenceStatusDict;
use App\Entity\User;
use App\Service\DateService;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\BCryptPasswordEncoder;

/**
 * Created by PhpStorm.
 * User: TheKrzok
 * Date: 15.01.2018
 * Time: 02:16
 */

/**
 * Fixtura do tworzenia konta administracyjnego
 *
 * Class DefaultFixtures
 * @package App\DataFixtures
 */
class DefaultFixtures extends Fixture
{
    /**
     * @param ObjectManager $manager
     * @throws \Exception
     */
    public function load(ObjectManager $manager)
    {
        $admin = new User();
        $encoder = new BCryptPasswordEncoder("8");
        $dateService = new DateService();
        $admin->setName('Admin');
        $admin->setSurname('Admin');
        $admin->setBirthdate(new DateTime());
        $admin->setPasswordExpiryDate($dateService->getNowPlusMonth());
        $password = $encoder->encodePassword('admin', 'salt');
        $admin->setPassword($password);
        $admin->addRole('ROLE_ADMIN');
        $admin->setEmail('admin@system.com');
        $admin->setPesel('0000000000000');
        $admin->setUsername('admin');
        $admin->setIsActive(true);

        $manager->persist($admin);


        $asd1 = new AbsenceStatusDict();
        $asd1->setName('pending');
        $asd1->setDescription('oczekuje');

        $manager->persist($asd1);

        $asd2 = new AbsenceStatusDict();
        $asd2->setName('accepted');
        $asd2->setDescription('zaakceeptowano');

        $manager->persist($asd2);

        $asd3 = new AbsenceStatusDict();
        $asd3->setName('rejected');
        $asd3->setDescription('odrzucono');

        $manager->persist($asd3);


        $manager->flush();
    }
}