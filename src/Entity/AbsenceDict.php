<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AbsenceDictRepository")
 * @UniqueEntity(fields="description", message="Nieobecność o podanym typie już istnieje")
 */
class AbsenceDict
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @ORM\Column(name="description", type="string", length=30, nullable=true, unique=true)
     * @var string|null
     */
    private $description;

    /**
     * @ORM\Column(name="code", type="string", length=5, nullable=true, unique=true)
     * @var null|string
     */
    private $code;

    /**
     * @return null|string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param null|string $description
     */
    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    /**
     * @ORM\Column(name="limitable", type="boolean", nullable=true)
     * @var boolean|null
     */
    private $limitable;

    /**
     * @return bool|null
     */
    public function getLimitable(): ?bool
    {
        return $this->limitable;
    }

    /**
     * @param bool|null $limitable
     */
    public function setLimitable(?bool $limitable): void
    {
        $this->limitable = $limitable;
    }

    /**
     * @ORM\Column(name="active", type="boolean", nullable=true)
     * @var bool|null
     */
    private $active;

    /**
     * @return bool|null
     */
    public function getActive(): ?bool
    {
        return $this->active;
    }

    /**
     * @param bool|null $active
     */
    public function setActive(?bool $active): void
    {
        $this->active = $active;
    }

    /**
     * @return null|string
     */
    public function getCode(): ?string
    {
        return $this->code;
    }

    /**
     * @param null|string $code
     */
    public function setCode(?string $code): void
    {
        $this->code = $code;
    }


    /**
     * @ORM\Column(type="string", name="color", nullable=true)
     * @var null|string
     */
    private $color;

    /**
     * @return null|string
     */
    public function getColor(): ?string
    {
        return $this->color;
    }

    /**
     * @param null|string $color
     */
    public function setColor(?string $color): void
    {
        $this->color = $color;
    }


}
