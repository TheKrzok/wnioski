<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CalendarDayDictRepository")
 */
class CalendarDayDict
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @ORM\Column(type="string", length=3, nullable=false)
     * @var string
     */
    private $dayType;

    /**
     * @return string
     */
    public function getDayType(): string
    {
        return $this->dayType;
    }

    /**
     * @param string $dayType
     */
    public function setDayType(string $dayType): void
    {
        $this->dayType = $dayType;
    }


    /**
     * @ORM\Column(type="string", length=150, nullable=false)
     * @var string
     */
    private $description;

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }


}
