<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CalendarDayRepository")
 * @UniqueEntity(fields="day")
 */
class CalendarDay
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @ORM\OrderBy({"day" = "ASC"})
     */
    private $id;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Calendar")
     * @var Calendar
     */
    private $calendar;

    /**
     * @return Calendar
     */
    public function getCalendar(): Calendar
    {
        return $this->calendar;
    }

    /**
     * @param Calendar $calendar
     */
    public function setCalendar(Calendar $calendar): void
    {
        $this->calendar = $calendar;
    }


    /**
     * @ORM\Column(name="month", type="integer", nullable=false)
     * @var int
     */
    private $month;

    /**
     * @return int
     */
    public function getMonth(): int
    {
        return $this->month;
    }

    /**
     * @param int $month
     */
    public function setMonth(int $month): void
    {
        $this->month = $month;
    }

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\CalendarDayDict")
     * @var CalendarDayDict
     */
    private $dayType;

    /**
     * @return CalendarDayDict
     */
    public function getDayType(): CalendarDayDict
    {
        return $this->dayType;
    }

    /**
     * @param CalendarDayDict $dayType
     */
    public function setDayType(CalendarDayDict $dayType): void
    {
        $this->dayType = $dayType;
    }


    /**
     * @ORM\Column(type="date", nullable=false)
     * @var \DateTime
     */
    private $day;

    /**
     * @return \DateTime
     */
    public function getDay(): \DateTime
    {
        return $this->day;
    }

    /**
     * @param \DateTime $day
     */
    public function setDay(\DateTime $day): void
    {
        $this->day = $day;
    }

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\HolidayDict")
     * @var HolidayDict|null
     */
    private $holiday;

    /**
     * @ORM\Column(name="week_day", type="integer", nullable=false)
     * @var int
     */
    private $weekDay;

    /**
     * @return int
     */
    public function getWeekDay(): int
    {
        return $this->weekDay;
    }

    /**
     * @param int $weekDay
     */
    public function setWeekDay(int $weekDay): void
    {
        $this->weekDay = $weekDay;
    }


    /**
     * @ORM\Column(type="integer", name="week", nullable=false)
     * @var int
     */
    private $week;

    /**
     * @return int
     */
    public function getWeek(): int
    {
        return $this->week;
    }

    /**
     * @param int $week
     */
    public function setWeek(int $week): void
    {
        $this->week = $week;
    }


    /**
     * @ORM\Column(type="integer", name="day_number", nullable=false)
     * @var int
     */
    private $dayNumber;

    /**
     * @return int
     */
    public function getDayNumber(): int
    {
        return $this->dayNumber;
    }

    /**
     * @param int $dayNumber
     */
    public function setDayNumber(int $dayNumber): void
    {
        $this->dayNumber = $dayNumber;
    }


    /**
     * @ORM\Column(type="integer", name="quarter", nullable=false)
     * @var int
     */
    private $quarter;

    /**
     * @return int
     */
    public function getQuarter(): int
    {
        return $this->quarter;
    }

    /**
     * @param int $quarter
     */
    public function setQuarter(int $quarter): void
    {
        $this->quarter = $quarter;
    }

    /**
     * @return HolidayDict|null
     */
    public function getHoliday(): ?HolidayDict
    {
        return $this->holiday;
    }

    /**
     * @param HolidayDict|null $holiday
     */
    public function setHoliday(?HolidayDict $holiday): void
    {
        $this->holiday = $holiday;
    }

}
