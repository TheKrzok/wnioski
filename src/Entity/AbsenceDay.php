<?php

namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AbsenceDayRepository")
 */
class AbsenceDay
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Absence", inversedBy="absenceDays")
     * @var Absence
     */
    private $absence;

    /**
     * @ORM\Column(type="date", nullable=false)
     * @var DateTime
     */
    private $absenceDay;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Absence
     */
    public function getAbsence(): Absence
    {
        return $this->absence;
    }

    /**
     * @param Absence $absence
     */
    public function setAbsence(Absence $absence): void
    {
        $this->absence = $absence;
    }

    /**
     * @return DateTime
     */
    public function getAbsenceDay(): DateTime
    {
        return $this->absenceDay;
    }

    /**
     * @param DateTime $absenceDay
     */
    public function setAbsenceDay(DateTime $absenceDay): void
    {
        $this->absenceDay = $absenceDay;
    }
}
