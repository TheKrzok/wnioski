<?php

namespace App\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToMany;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Encja zawierająca dane użytkownika
 *
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @UniqueEntity(fields="email", message="Podany adres email został już użyty")
 * @UniqueEntity(fields="username", message="Nazwa użytkownika jest już zajęta")
 * @UniqueEntity(fields="pesel", message="Podany pesel został już użyty")
 */
class User implements AdvancedUserInterface, \Serializable
{
    /**
     * User constructor.
     */
    public function __construct()
    {
        $this->userRole     = new ArrayCollection();
        $this->userFiles    = new ArrayCollection();
        $this->absence      = new ArrayCollection();
        $this->absenceLimit = new ArrayCollection();
        $this->inferior     = new ArrayCollection();
    }


    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var int
     */
    private $id;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @OneToMany(targetEntity="App\Entity\User", mappedBy="superior")
     * @ORM\OrderBy({"surname" = "ASC"})
     */
    private $inferior;

    /**
     * @return mixed
     */
    public function getInferior()
    {
        return $this->inferior;
    }

    /**
     * @param mixed $inferior
     */
    public function setInferior($inferior): void
    {
        $this->inferior = $inferior;
    }

    /**
     * @ManyToOne(targetEntity="App\Entity\User", inversedBy="inferior")
     * @JoinColumn(name="superior_id", referencedColumnName="id")
     */
    private $superior;

    /**
     * @return mixed
     */
    public function getSuperior()
    {
        return $this->superior;
    }

    /**
     * @param mixed $superior
     */
    public function setSuperior($superior): void
    {
        $this->superior = $superior;
    }

    /**
     * @ManyToOne(targetEntity="App\Entity\Groups", inversedBy="user")
     *
     * @var Groups|null
     */
    private $group;

    /**
     * @return Groups|null
     */
    public function getGroup(): ?Groups
    {
        return $this->group;
    }

    /**
     * @param Groups|null $group
     */
    public function setGroup(?Groups $group): void
    {
        $this->group = $group;
    }

    /**
     * @var UserRole[]
     *
     * @ManyToMany(targetEntity="App\Entity\UserRole", inversedBy="user", cascade={"all"})
     * @JoinTable(name="users_roles",
     *      joinColumns={@JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@JoinColumn(name="role_id", referencedColumnName="id", unique=true)}
     *     )
     */
    private $userRole;

    /**
     * @var UserFiles[]
     *
     * @ORM\OneToMany(targetEntity="App\Entity\UserFiles", mappedBy="user", cascade={"all"})
     */
    private $userFiles;

    /**
     * @var Absence[]
     *
     * @OneToMany(targetEntity="App\Entity\Absence", mappedBy="user", cascade={"all"})
     */
    private $absence;

    /**
     * @return Absence[]
     */
    public function getAbsence(): array
    {
        return $this->absence;
    }

    /**
     * @param Absence[] $absence
     */
    public function setAbsence(array $absence): void
    {
        $this->absence = $absence;
    }

    /**
     * @param Absence $absence
     */
    public function addAbsence(Absence $absence)
    {
        $this->absence[] = $absence;
    }

    /**
     * @var AbsenceLimit[]
     *
     * @OneToMany(targetEntity="App\Entity\AbsenceLimit", mappedBy="user", cascade={"all"})
     * @ORM\OrderBy({"year" = "ASC",})
     *
     */
    private $absenceLimit;

    /**
     * @return AbsenceLimit[]|ArrayCollection
     */
    public function getAbsenceLimit()
    {
        return $this->absenceLimit;
    }

    /**
     * @param AbsenceLimit[] $absenceLimit
     */
    public function setAbsenceLimit(array $absenceLimit): void
    {
        $this->absenceLimit = $absenceLimit;
    }

    /**
     * @ORM\Column(name="name", type="string", length=30)
     * @var string|null
     */
    private $name;

    /**
     * @ORM\Column(name="surname", type="string", length=50)
     * @var string|null
     */
    private $surname;

    /**
     * @ORM\Column(name="email", type="string", length=255, unique=true)
     * @var string|null
     */
    private $email;

    /**
     * @ORM\Column(name="pesel", type="string", length=13, unique=true)
     * @var string|null
     */
    private $pesel;

    /**
     * @ORM\Column(name="birthdate", type="datetime")
     * @var DateTime|null
     */
    private $birthdate;

    /**
     * @ORM\Column(name="username", type="string", length=30, unique=true)
     * @var string|null
     */
    private $username;

    /**
     * @var string
     */
    private $plainPassword;

    /**
     * @ORM\Column(name="password", type="string", length=64)
     * @var string
     */
    private $password;

    /**
     *
     * @ORM\Column(name="password_expiry_date", type="datetime", nullable=true)
     * @var DateTime
     */
    private $passwordExpiryDate;

    /**
     * @ORM\Column(name="activation_code", type="string", length=64, nullable=true)
     * @var string
     */
    private $activationCode;

    /**
     * @ORM\Column(name="is_active", type="boolean", nullable=false, options={"default"=true})
     * @var boolean
     */
    private $isActive;

    /**
     * @ORM\Column(name="resetting_security_code", type="string", nullable=true)
     * @var string
     */
    private $resettingSecurityCode;

    /**
     * @ORM\Column(type="string", name="color", nullable=true)
     * @var string|null
     */
    private $color;

    /**
     * @return string
     */
    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    /**
     * @param string $password
     */
    public function setPlainPassword(string $password): void
    {
        $this->plainPassword = $password;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword(string $password): void
    {
        $this->password = $password;
    }

    /**
     * @return DateTime
     */
    public function getPasswordExpiryDate()
    {
        return $this->passwordExpiryDate;
    }

    /**
     * @param DateTime $passwordExpiryDate
     */
    public function setPasswordExpiryDate(DateTime $passwordExpiryDate): void
    {
        $this->passwordExpiryDate = $passwordExpiryDate;
    }

    /**
     * @return string
     */
    public function getActivationCode()
    {
        return $this->activationCode;
    }

    /**
     * @param string $activationCode
     */
    public function setActivationCode(string $activationCode): void
    {
        $this->activationCode = $activationCode;
    }

    /**
     * @return string
     */
    public function getResettingSecurityCode()
    {
        return $this->resettingSecurityCode;
    }

    /**
     * @param null|string $resettingSecurityCode
     */
    public function setResettingSecurityCode(?string $resettingSecurityCode): void
    {
        $this->resettingSecurityCode = $resettingSecurityCode;
    }

    /**
     * @return null|string
     */
    public function getSalt()
    {
        return null;
    }

    /**
     * @return array
     */
    public function getRoles()
    {
        $userRole = array();
        $userRoleObjectArray = $this->getUserRole();
        foreach($userRoleObjectArray AS $ur)
        {
            array_push($userRole, $ur->getRole());
        }
        return $userRole;
    }

    /**
     * @return bool
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * @param bool $isActive
     */
    public function setIsActive(bool $isActive): void
    {
        $this->isActive = $isActive;
    }

    /**
     * @return bool
     */
    public function isAccountNonExpired()
    {
        return true;
    }

    /**
     * @return bool
     */
    public function isAccountNonLocked()
    {
        return true;
    }

    /**
     * @return bool
     */
    public function isCredentialsNonExpired()
    {
        return false;
    }

    /**
     * @return bool
     */
    public function isEnabled()
    {
        return $this->isActive;
    }

    /**
     * @return void
     */
    public function eraseCredentials()
    {

    }

    /** @see \Serializable::serialize() */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->username,
            $this->password,
            $this->isActive,
        ));
    }

    /** @see \Serializable::unserialize() */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->username,
            $this->password,
            $this->isActive,
            ) = unserialize($serialized);
    }

    /**
     * @return UserRole[]|ArrayCollection
     */
    public function getUserRole()
    {
        return $this->userRole;
    }

    /**
     * @param UserRole[] $userRole
     */
    public function setUserRole(array $userRole): void
    {
        $this->userRole = $userRole;
    }

    /**
     * @param string $role
     */
    public function addRole(string $role)
    {
        $newRole = new UserRole();
        $newRole->setRole($role);
        $this->userRole->add($newRole);
    }

    /**
     * @return UserFiles[]|ArrayCollection
     */
    public function getUserFiles()
    {
        return $this->userFiles;
    }

    /**
     * @param UserFiles[] $userFiles
     */
    public function setUserFiles(array $userFiles): void
    {
        $this->userFiles = $userFiles;
    }

    public function addFile(string $originalName, string $filePath)
    {
        $newFile = new UserFiles();
        $newFile->setOriginalName($originalName);
        $newFile->setFilePath($filePath);
        $newFile->setUser($this);
        $this->userFiles->add($newFile);
    }

    /**
     * @return string
     */
    public function getColor(): string
    {
        return $this->color;
    }

    /**
     * @param string $color
     */
    public function setColor(?string $color): void
    {
        $this->color = $color;
    }


    /**
     * @return string
     */
    public function getFullName()
    {
        return $this->surname.' '.$this->name;
    }


    /**
     * @return string
     */
    public function __toString() {
        return $this->id;
    }

    /**
     * @return null|string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param null|string $name
     */
    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return null|string
     */
    public function getSurname(): ?string
    {
        return $this->surname;
    }

    /**
     * @param null|string $surname
     */
    public function setSurname(?string $surname): void
    {
        $this->surname = $surname;
    }

    /**
     * @return null|string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param null|string $email
     */
    public function setEmail(?string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return null|string
     */
    public function getPesel(): ?string
    {
        return $this->pesel;
    }

    /**
     * @param null|string $pesel
     */
    public function setPesel(?string $pesel): void
    {
        $this->pesel = $pesel;
    }

    /**
     * @return DateTime|null
     */
    public function getBirthdate(): ?DateTime
    {
        return $this->birthdate;
    }

    /**
     * @param DateTime|null $birthdate
     */
    public function setBirthdate(?DateTime $birthdate): void
    {
        $this->birthdate = $birthdate;
    }

    /**
     * @return null|string
     */
    public function getUsername(): ?string
    {
        return $this->username;
    }

    /**
     * @param null|string $username
     */
    public function setUsername(?string $username): void
    {
        $this->username = $username;
    }
}