<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CalendarRepository")
 */
class Calendar
{

    public function __construct()
    {
        $this->holidayDict  = new ArrayCollection();
        $this->calendarDays = new ArrayCollection();
    }

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @ORM\Column(type="integer", name="year", nullable=false)
     * @var int
     */
    private $year;

    /**
     * @return int
     */
    public function getYear(): int
    {
        return $this->year;
    }

    /**
     * @param int $year
     */
    public function setYear(int $year): void
    {
        $this->year = $year;
    }

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\HolidayDict", mappedBy="calendar", cascade={"all"})
     * @var ArrayCollection
     */
    private $holidayDict;

    /**
     * @return Collection
     */
    public function getHolidayDict(): Collection
    {
        return $this->holidayDict;
    }

    /**
     * @param Collection $holidayDict
     */
    public function setHolidayDict(Collection $holidayDict): void
    {
        $this->holidayDict = $holidayDict;
    }


    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CalendarDay", mappedBy="calendar", cascade={"all"})
     * @var ArrayCollection
     */
    private $calendarDays;

    /**
     * @return Collection
     */
    public function getCalendarDays(): Collection
    {
        return $this->calendarDays;
    }

    /**
     * @param Collection $calendarDays
     */
    public function setCalendarDays(Collection $calendarDays): void
    {
        $this->calendarDays = $calendarDays;
    }

}
