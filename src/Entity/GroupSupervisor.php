<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\GroupSupervisorRepository")
 */
class GroupSupervisor
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @var int
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @var User
     */
    private $supervisor;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Groups")
     * @var Groups
     */
    private $group;

    /**
    * @return int
    */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return User
     */
    public function getSupervisor(): User
    {
        return $this->supervisor;
    }

    /**
     * @param User $supervisor
     */
    public function setSupervisor(User $supervisor): void
    {
        $this->supervisor = $supervisor;
    }

    /**
     * @return Groups
     */
    public function getGroup(): Groups
    {
        return $this->group;
    }

    /**
     * @param Groups $group
     */
    public function setGroup(Groups $group): void
    {
        $this->group = $group;
    }


}
