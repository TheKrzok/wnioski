<?php
/**
 * Created by PhpStorm.
 * User: TheKrzok
 * Date: 02.02.2018
 * Time: 11:19
 */

namespace App\Twig;

use Twig_Extension;
use Twig_SimpleFunction;

class AbsenceExtension extends Twig_Extension
{
    /**
     * @return array
     */
    public function getFunctions()
    {
        return [
            new Twig_SimpleFunction("statusStyle", [$this, "statusStyle"]),
            new Twig_SimpleFunction("statusStyleMail", [$this, "statusStyleMail"])
        ];
    }

    /**
     * @param string $status
     * @return string
     */
    public function statusStyle(string $status)
    {
        if ($status === 'accepted') {
            return "badge badge-success";
        } else if($status === 'rejected') {
            return "badge badge-danger";
        } else if($status === 'pending') {
            return "badge badge-info";
        } else if($status === 'initial') {
            return "badge badge-primary";
        } else if($status === 'cancelled') {
            return "badge badge-dark";
        } else if($status === 'withdrawn') {
            return "badge badge-secondary";
        }
        return "";
    }

    public function statusStyleMail(string $status)
    {
        if ($status === 'accepted') {
            return "#28a745";
        } else if($status === 'rejected') {
            return "#dc3545";
        } else if($status === 'pending') {
            return "#6c757d";
        } else if($status === 'initial') {
            return "#007bff";
        } else if($status === 'cancelled') {
            return "#343a40";
        } else if($status === 'withdrawn') {
            return "#6c757d";
        }
        return "";
    }
}